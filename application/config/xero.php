<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Configuration options for Xero private application
 */

$config = array(
    'consumer'	=> array(
        'key'		=> 'PHYHM01IWMR8OYZXER99DSOGSNKJZ0',
        'secret'	=> 'F4GM6SWR1KPIENSXIMSKNMS2GIYCPF'
    ),
    'certs'		=> array(
        'private'  	=> APPPATH.'/licences_key/privatekey.pem',
        'public'  	=> APPPATH.'/licences_key/publickey.cer'
    ),
    'format'    => 'json'
);