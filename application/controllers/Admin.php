<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 *  @author     : Creativeitem
 *  date        : 14 september, 2017
 *  Ekattor School Management System Pro
 *  http://codecanyon.net/user/Creativeitem
 *  http://support.creativeitem.com
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Barcode_model');
	    $this->load->library('ci_qr_code');
        $this->config->load('qr_code');
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /*** default function, redirects to login page if no admin logged in yet ***/

    public function index() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
    }
    
    /*** ADMIN DASHBOARD ***/

    function dashboard() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $user_id = $this->session->userdata('login_user_id');
        $user_query = "SELECT * FROM `admin` WHERE admin_id = $user_id";
        $user_data = $this->db->query($user_query);

        $this->session->set_userdata('user_data',$user_data->row());
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('admin_dashboard');
        $this->load->view('backend/index', $page_data);
    }

    public function deauth_google(){

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $user_id = $this->session->userdata('login_user_id');
        $user_query = "SELECT * FROM `admin` WHERE admin_id = $user_id";
        $user_data = $this->db->query($user_query)->result_array();
        
        if(count($user_data) != 0){
            $update_query = "UPDATE `admin` SET `access_token` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $update_query = "UPDATE `admin` SET `scope` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $update_query = "UPDATE `admin` SET `refresh_token` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $update_query = "UPDATE `admin` SET `expires` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $update_query = "UPDATE `admin` SET `token_type` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $update_query = "UPDATE `admin` SET `token_created` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $update_query = "UPDATE `admin` SET `access_json` = '' WHERE admin_id = $user_id";
            $update_query = $this->db->query($update_query);

            $this->session->set_userdata('user_data',"");
            
            redirect(base_url(), 'refresh');
        }
    }

    public function auth_google(){

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $redirect_uri = base_url().'index.php?admin/auth_google/';
        $clientSecretPath = __DIR__.'/googleconfig/client_secret.json';
        
        $client = new Google_Client();
        $client->setApplicationName('MyBusiness');
        $client->setAuthConfigFile($clientSecretPath);
        $client->addScope("https://www.googleapis.com/auth/business.manage");
        $client->setRedirectUri($redirect_uri);
        // For retrieving the refresh token
        $client->setAccessType("offline");
        $client->setApprovalPrompt("force");
        /************************************************
        We are going to create the Google My Business API
        service, and query it.
        ************************************************/
        //include_once "Mybusiness.php";
        //$mybusinessService = new Google_Service_Mybusiness($client);
        $user_id = $this->session->userdata('login_user_id');
        
        $user_query = "SELECT * FROM `admin` WHERE admin_id = $user_id";
        $user_data = $this->db->query($user_query)->result_array();
        
        if(count($user_data) != 0){
           
            if (isset($_GET['code'])) {
                
                // Exchange authorization code for an access token.
                $accessToken = $client->authenticate($_GET['code']);
                
                $update_query = "UPDATE `admin` SET `access_token` = '".$accessToken['access_token']."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
    
                $update_query = "UPDATE `admin` SET `scope` = '".$accessToken['scope']."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
    
                $update_query = "UPDATE `admin` SET `refresh_token` = '".$accessToken['refresh_token']."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
    
                $update_query = "UPDATE `admin` SET `expires` = '".$accessToken['expires_in']."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
    
                $update_query = "UPDATE `admin` SET `token_type` = '".$accessToken['token_type']."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
    
                $update_query = "UPDATE `admin` SET `token_created` = '".$accessToken['created']."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
    
                $update_query = "UPDATE `admin` SET `access_json` = '".json_encode($accessToken)."' WHERE admin_id = $user_id";
                $update_query = $this->db->query($update_query);
                
                redirect(base_url(), 'refresh');
            }
            
            // Load previously authorized credentials from a file.
            if ($user_data[0]['access_token'] != "") {
                $accessToken = $user_data[0]['access_json'];
                
                $client->setAccessToken($accessToken);
                // Refresh the token if it's expired.
                if ($client->isAccessTokenExpired()) {
                    $accessToken = $client->refreshToken($client->getRefreshToken());
                    
                    $update_query = "UPDATE `admin` SET `access_token` = '".$accessToken['access_token']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `scope` = '".$accessToken['scope']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `refresh_token` = '".$accessToken['refresh_token']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `expires` = '".$accessToken['expires_in']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `token_type` = '".$accessToken['token_type']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `token_created` = '".$accessToken['created']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `access_json` = '".json_encode($accessToken)."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
                    
                }
                redirect(base_url(), 'refresh');
            } else {
                
                $authUrl = $client->createAuthUrl();
                header('Location: ' . $authUrl);
            }

        }
    }
    
    public function google_reoauth(){
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $user_id = $this->session->userdata('login_user_id');
        
        $user_query = "SELECT * FROM `admin` WHERE admin_id = $user_id";
        $user_data = $this->db->query($user_query)->result_array();

        $redirect_uri = base_url().'index.php?admin/auth_google/';
        $clientSecretPath = __DIR__.'/googleconfig/client_secret.json';
        
        $client = new Google_Client();
        $client->setApplicationName('MyBusiness');
        $client->setAuthConfigFile($clientSecretPath);
        $client->addScope("https://www.googleapis.com/auth/business.manage");
        $client->setRedirectUri($redirect_uri);
        // For retrieving the refresh token
        $client->setAccessType("offline");
        $client->setApprovalPrompt("force");

        if(count($user_data) != 0){

            if ($user_data[0]['access_token'] != "") {
                $accessToken = $user_data[0]['access_json'];
                
                $client->setAccessToken($accessToken);
                // Refresh the token if it's expired.
                if ($client->isAccessTokenExpired()) {
                    $accessToken = $client->refreshToken($client->getRefreshToken());
                    
                    $update_query = "UPDATE `admin` SET `access_token` = '".$accessToken['access_token']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `scope` = '".$accessToken['scope']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `refresh_token` = '".$accessToken['refresh_token']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `expires` = '".$accessToken['expires_in']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `token_type` = '".$accessToken['token_type']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `token_created` = '".$accessToken['created']."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
        
                    $update_query = "UPDATE `admin` SET `access_json` = '".json_encode($accessToken)."' WHERE admin_id = $user_id";
                    $update_query = $this->db->query($update_query);
                    
                }
                return $client;
            } else {
                
                $authUrl = $client->createAuthUrl();
                header('Location: ' . $authUrl);
            }
        }
    }

    public function get_business(){
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        //$mybusinessService = new Google_Service_Mybusiness($client);
        $client = $this->google_reoauth();

        //$mybusinessService = new Google_Service_MyBusinessLodging_Business($client);
        //$accounts = $mybusinessService->getBusinessCenter();

        $gmbService = new Google_Service_MyBusiness($client);
        $accounts = $gmbService->accounts->listAccounts();
        //$result = (array)$results;
        $account = json_decode(json_encode($accounts), true);
        //echo "<pre>";
        //print_r($result['accounts'][0]['name']);
        //echo "</pre>";
        //echo $result['modelData'];
        
        $locations = $gmbService->accounts_locations->listAccountsLocations($accounts['accounts'][0]['name']);
        $location = json_decode(json_encode($locations), true);
        
        $page_data['locations'] = $location['locations'];
        $page_data['page_name'] = 'account_locations';
        $page_data['page_title'] = get_phrase('my_business');
        $this->load->view('backend/index', $page_data);
    }

    public function get_all_reviews(){
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        //$mybusinessService = new Google_Service_Mybusiness($client);
        $client = $this->google_reoauth();


        //$mybusinessService = new Google_Service_MyBusinessLodging_Business($client);
        //$accounts = $mybusinessService->getBusinessCenter();

        $gmbService = new Google_Service_MyBusiness($client);
        $accounts = $gmbService->accounts->listAccounts();
        //$result = (array)$results;
        $account = json_decode(json_encode($accounts), true);
        
        $locations = $gmbService->accounts_locations->listAccountsLocations($accounts['accounts'][0]['name']);
        $location = json_decode(json_encode($locations), true);
        
        $allReviews = array();
        $locationName = array();
        
        for($i=0;$i<count($location['locations']);$i++){
            $reviews = $gmbService->accounts_locations_reviews->listAccountsLocationsReviews($location['locations'][$i]['name']);
            $review = json_decode(json_encode($reviews), true);
            array_push($locationName,$location['locations'][$i]);
            if(is_array($review['reviews'])){
                array_push($allReviews,$review['reviews']);
            }
        }
        
        //echo "<pre>";
        //print_r($locationName);
        //echo "</pre>";
        //die;
        $page_data['allReviews'] = $allReviews;
        $page_data['locationName'] = $locationName;
        $page_data['page_name'] = 'account_reviews';
        $page_data['page_title'] = get_phrase('reviews');
        $this->load->view('backend/index', $page_data);
    }
    
    public function post_reply(){
        
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $reviewID = $_POST['reviewID'];
        $comment = $_POST['comment'];
        
        //echo $reviewID;die;
        
        $client = $this->google_reoauth();
        
        $gmbService = new Google_Service_MyBusiness($client);
        
        $postBody = new Google_Service_MyBusiness_ReviewReply();
        $postBody->setComment($comment);
        $comment = $postBody;
        
        $reply = $gmbService->accounts_locations_reviews->updateReply($reviewID,$comment);
        
        $this->session->set_flashdata('flash_message', get_phrase('reply_posted_successfully'));
        redirect(base_url() . 'index.php?admin/get_all_reviews', 'refresh');
    }
    
    public function review_reply(){
        
        
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $reviewID = $_GET[0];
        $page_data['review_id'] = $reviewID;
        $page_data['page_name'] = 'review_reply';
        $page_data['page_title'] = get_phrase('reply');
        $this->load->view('backend/index', $page_data);
        
    }
    
    /**** MANAGE Users *****/

    function users($param1 = '', $param2 = '', $param3 = '') {

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['password'] = sha1($this->input->post('password'));
            $data['level'] = $this->input->post('level');
            $data['created_by'] = $this->session->userdata('login_user_id');
            $data['updated_by'] = $this->session->userdata('login_user_id');
            $data['created_date'] = date("Y-m-d");
            $data['updated_date'] = date("Y-m-d");

            if ($this->input->post('phone') != null) {
                $data['phone'] = $this->input->post('phone');
            }
            if ($this->input->post('designation') != null) {
                $data['designation'] = $this->input->post('designation');
            }
            
            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('admin', $data);
                $staff_id = $this->db->insert_id();
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                //$this->email_model->account_opening_email('admin', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(base_url() . 'index.php?admin/users/', 'refresh');
        }

        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');

            if ($this->input->post('phone') != null) {
                $data['phone'] = $this->input->post('phone');
            } else {
                $data['phone'] = null;
            }
            if ($this->input->post('designation') != null) {
                $data['designation'] = $this->input->post('designation');
            } else {
                $data['designation'] = null;
            }
            if ($this->input->post('password') != null) {
                $data['password'] = sha1($this->input->post('password'));
            }
            $data['updated_by'] = $this->session->userdata('login_user_id');
            $data['updated_date'] = date("Y-m-d");

            $validation = email_validation_for_edit($data['email'], $param2, 'admin');
            if ($validation == 1) {
                $this->db->where('admin_id', $param2);
                $this->db->update('admin', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(base_url() . 'index.php?admin/users/', 'refresh');
        }

        if ($param1 == 'delete') {
            $data['status'] = 0;
            $data['updated_by'] = $this->session->userdata('login_user_id');
            $data['updated_date'] = date("Y-m-d");

            $this->db->where('admin_id', $param2);
            $this->db->update('admin', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/users/', 'refresh');
        }

        $page_data['staff'] = $this->db->get_where('admin',array('status'=>1))->result_array();
        $page_data['page_name'] = 'users';
        $page_data['page_title'] = get_phrase('manage_users');
        $this->load->view('backend/index', $page_data);
    }

    
    /***** SITE/SYSTEM SETTINGS *********/

    function system_settings($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'do_update') {
            $data['description'] = $this->input->post('system_name');
            $this->db->where('type', 'system_name');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('system_title');
            $this->db->where('type', 'system_title');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('address');
            $this->db->where('type', 'address');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('phone');
            $this->db->where('type', 'phone');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('paypal_email');
            $this->db->where('type', 'paypal_email');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('payumoney_merchant_key');
            $this->db->where('type', 'payumoney_merchant_key');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('payumoney_salt_id');
            $this->db->where('type', 'payumoney_salt_id');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('currency');
            $this->db->where('type', 'currency');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('system_email');
            $this->db->where('type', 'system_email');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('system_name');
            $this->db->where('type', 'system_name');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('language');
            $this->db->where('type', 'language');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('text_align');
            $this->db->where('type', 'text_align');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('running_year');
            $this->db->where('type', 'running_year');
            $this->db->update('settings', $data);
            $data['description'] = $this->input->post('purchase_code');
            $this->db->where('type', 'purchase_code');
            $this->db->update('settings', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        if ($param1 == 'upload_logo') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        if ($param1 == 'change_skin') {
            $data['description'] = $param2;
            $this->db->where('type', 'skin_colour');
            $this->db->update('settings', $data);
            $this->session->set_flashdata('flash_message', get_phrase('theme_selected'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        $page_data['page_name'] = 'system_settings';
        $page_data['page_title'] = get_phrase('system_settings');
        $page_data['settings'] = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }


    /****** MANAGE OWN PROFILE AND CHANGE PASSWORD ***/

    function manage_profile($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($param1 == 'update_profile_info') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $admin_id = $param2;
            $validation = email_validation_for_edit($data['email'], $admin_id, 'admin');
            if ($validation == 1) {
                $this->db->where('admin_id', $this->session->userdata('admin_id'));
                $this->db->update('admin', $data);
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/admin_image/' . $this->session->userdata('admin_id') . '.jpg');
                $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }
            redirect(base_url() . 'index.php?admin/manage_profile/', 'refresh');
        }
        if ($param1 == 'change_password') {
            $data['password'] = sha1($this->input->post('password'));
            $data['new_password'] = sha1($this->input->post('new_password'));
            $data['confirm_new_password'] = sha1($this->input->post('confirm_new_password'));

            $current_password = $this->db->get_where('admin', array(
                        'admin_id' => $this->session->userdata('admin_id')
                    ))->row()->password;
            if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {
                $this->db->where('admin_id', $this->session->userdata('admin_id'));
                $this->db->update('admin', array(
                    'password' => $data['new_password']
                ));
                $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('password_mismatch'));
            }
            redirect(base_url() . 'index.php?admin/manage_profile/', 'refresh');
        }
        $page_data['page_name'] = 'manage_profile';
        $page_data['page_title'] = get_phrase('manage_profile');
        $page_data['edit_data'] = $this->db->get_where('admin', array(
                    'admin_id' => $this->session->userdata('admin_id')
                ))->result_array();
        $this->load->view('backend/index', $page_data);
    }
}
