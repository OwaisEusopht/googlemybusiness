<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 *  @author   : Creativeitem
 *  date    : 14 september, 2017
 *  Ekattor School Management System Pro
 *  http://codecanyon.net/user/Creativeitem
 *  http://support.creativeitem.com
 */

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    }

    public function renderJSON($jsonData) {
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($jsonData));
    }

    //Default function, redirects to logged in user area
    public function index() {

        if ($this->session->userdata('login_type') && $this->session->userdata('admin_login') == 1){
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
        }
        $this->load->view('backend/login2');
    }

    //Validating login from ajax request
    function validate_login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $credential = array('email' => $email, 'password' => sha1($password));
        // Checking login credential for admin
        $query = $this->db->get_where('admin', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            
            $this->session->set_userdata('admin_login', '1');
            $this->session->set_userdata('admin_id', $row->admin_id);
            $this->session->set_userdata('login_user_id', $row->admin_id);
            $this->session->set_userdata('name', $row->name);
            $this->session->set_userdata('login_type', 'admin');
            $this->session->set_userdata('level', $row->level);
            $this->session->set_userdata('user_data',$row);
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
        }

        $this->session->set_flashdata('login_error', get_phrase('invalid_login'));
        redirect(base_url() . 'index.php?login', 'refresh');
    }

    function ajax_login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $credential = array('email' => $email, 'password' => sha1($password));
        // Checking login credential for admin
        $query = $this->db->get_where('admin', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->session->set_userdata('admin_login', '1');
            $this->session->set_userdata('admin_id', $row->admin_id);
            $this->session->set_userdata('login_user_id', $row->admin_id);
            $this->session->set_userdata('name', $row->name);
            $this->session->set_userdata('login_type', 'admin');
            $this->session->set_userdata('level', $row->level);
            $this->session->set_userdata('user_data',$row);
            //redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
            //echo "success";
            $response['redirect_url'] = "index.php?admin/dashboard";
            $response['login_status'] = 'success';
        }
        else{
            $response['login_status'] = 'invalid';
        }

        $this->renderJSON($response);
    }

function ajax_forgot_password() {

        $old_pass = $this->input->post('old_pass');
        $new_pass = $this->input->post('new_pass');
        $repeat_pass = $this->input->post('repeat_pass');
        $user_id = $this->input->post('user_id');
        
        $credential = array('franchise_id' => $user_id, 'password' => sha1($old_pass));
        // Checking login credential for admin
        $query = $this->db->get_where('franchise', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //$this->session->set_userdata('level', $row->level);
            
            if($new_pass == $repeat_pass){
                $data['password'] = sha1($new_pass);
                $this->db->where('franchise_id', $user_id);
                $this->db->update('franchise', $data);
                $response['status'] = 'success';
            }else{
                $response['status'] = 'invalid2';
            }
            //$response['redirect_url'] = "index.php?admin/dashboard";
        }
        else{
            $response['status'] = 'invalid1';
        }
        //die();
        $this->renderJSON($response);
    }

    /***DEFAULT NOR FOUND PAGE**** */

    // PASSWORD RESET BY EMAIL
    function forgot_password() {
        $this->load->view('backend/forgot_password2');
    }

    function reset_password() {
        $email = $this->input->post('email');
        $reset_account_type = '';
        //resetting user password here
        $new_password = substr(md5(rand(100000000, 20000000000)), 0, 7);
        // Checking credential for admin
        $query = $this->db->get_where('admin', array('email' => $email));
        if ($query->num_rows() > 0) {
            $reset_account_type = 'admin';
            $this->db->where('email', $email);
            $this->db->update('admin', array('password' => sha1($new_password)));
            // send new password to user email
            $this->email_model->password_reset_email($new_password, $reset_account_type, $email);
            $this->session->set_flashdata('reset_success', get_phrase('please_check_your_email_for_new_password'));
            redirect(base_url() . 'index.php?login/forgot_password', 'refresh');
        }
        // Checking credential for student
        $query = $this->db->get_where('student', array('email' => $email));
        if ($query->num_rows() > 0) {
            $reset_account_type = 'student';
            $this->db->where('email', $email);
            $this->db->update('student', array('password' => sha1($new_password)));
            // send new password to user email
            $this->email_model->password_reset_email($new_password, $reset_account_type, $email);
            $this->session->set_flashdata('reset_success', get_phrase('please_check_your_email_for_new_password'));
            redirect(base_url() . 'index.php?login/forgot_password', 'refresh');
        }
        // Checking credential for teacher
        $query = $this->db->get_where('teacher', array('email' => $email));
        if ($query->num_rows() > 0) {
            $reset_account_type = 'teacher';
            $this->db->where('email', $email);
            $this->db->update('teacher', array('password' => sha1($new_password)));
            // send new password to user email
            $this->email_model->password_reset_email($new_password, $reset_account_type, $email);
            $this->session->set_flashdata('reset_success', get_phrase('please_check_your_email_for_new_password'));
            redirect(base_url() . 'index.php?login/forgot_password', 'refresh');
        }
        // Checking credential for parent
        $query = $this->db->get_where('parent', array('email' => $email));
        if ($query->num_rows() > 0) {
            $reset_account_type = 'parent';
            $this->db->where('email', $email);
            $this->db->update('parent', array('password' => sha1($new_password)));
            // send new password to user email
            $this->email_model->password_reset_email($new_password, $reset_account_type, $email);
            $this->session->set_flashdata('reset_success', get_phrase('please_check_your_email_for_new_password'));
            redirect(base_url() . 'index.php?login/forgot_password', 'refresh');
        }
        $this->session->set_flashdata('reset_error', get_phrase('password_reset_was_failed'));
        redirect(base_url() . 'index.php?login/forgot_password', 'refresh');
    }

    /*******LOGOUT FUNCTION ****** */

    function logout() {
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url() . 'index.php?login', 'refresh');
    }

}
