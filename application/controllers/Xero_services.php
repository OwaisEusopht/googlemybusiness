<?php

use GuzzleHttp\Client;
use XeroAPI\XeroPHP\Api\AccountingApi;
use XeroAPI\XeroPHP\Configuration;

class Xero_services extends CI_Controller {


    private $host = 'https://api.xero.com/api.xro/2.0';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('email_model');
        // $this->load->library('xero');

    }
    public function index() {


            $query = "SELECT * FROM contacts  WHERE contacts.status = 1";

            $contacts = $this->db->query($query)->result_array();
            print_r($contacts);
            die('ss');

        die('XERO');
// automatically instantiates the Xero class with your key, secret
// and paths to your RSA cert and key
// according to the configuration options you defined in appication/config/xero.php
        $this->load->library('xero');

// the input format for creating a new contact
// see http://blog.xero.com/developer/api/contacts/ to understand more
        $new_contact = array(
            array(
                "Name" => "API TEST Contact",
                "FirstName" => "TEST",
                "LastName" => "Contact",
                "Addresses" => array(
                    "Address" => array(
                        array(
                            "AddressType" => "POBOX",
                            "AddressLine1" => "PO Box 100",
                            "City" => "Someville",
                            "PostalCode" => "3890"
                        ),
                        array(
                            "AddressType" => "STREET",
                            "AddressLine1" => "1 Some Street",
                            "City" => "Someville",
                            "PostalCode" => "3890"
                        )
                    )
                )
            )
        );
// create the contact
        $contact_result = $this->xero->Contacts($new_contact);

// the input format for creating a new invoice (or credit note)
// see [http://blog.xero.com/developer/api/invoices/]
        $new_invoice = array(
            array(
                "Type"=>"ACCREC",
                "Contact" => array(
                    "Name" => "API TEST Contact"
                ),
                "Date" => "2010-04-08",
                "DueDate" => "2010-04-30",
                "Status" => "AUTHORISED",
                "LineAmountTypes" => "Exclusive",
                "LineItems"=> array(
                    "LineItem" => array(
                        array(
                            "Description" => "Just another test invoice",
                            "Quantity" => "2.0000",
                            "UnitAmount" => "250.00",
                            "AccountCode" => "200"
                        )
                    )
                )
            )
        );
// the input format for creating a new payment
// see [http://blog.xero.com/developer/api/payments/] to understand more
        $new_payment = array(
            array(
                "Invoice" => array(
                    "InvoiceNumber" => "INV-0002"
                ),
                "Account" => array(
                    "Code" => "[account code]"
                ),
                "Date" => "2010-04-09",
                "Amount"=>"100.00",
            )
        );


// raise an invoice
        $invoice_result = $this->xero->Invoices($new_invoice);

        $payment_result = $this->xero->Payments($new_payment);


// get details of an account, with the name "Test Account"
        $result = $this->xero->Accounts(false, false, array("Name"=>"Test Account"));
// the params above correspond to the "Optional params for GET Accounts"
// on http://blog.xero.com/developer/api/accounts/

// to do a POST request, the first and only param must be a
// multidimensional array as shown above in $new_contact etc.

// get details of all accounts
        $all_accounts = $this->xero->Accounts;

// echo the results back
        if (is_object($result)) {
// use this to see the source code if the $format option is "xml"
            echo htmlentities($result->asXML()) . "<hr />";
        } else {
// use this to see the source code if the $format option is "json" or not specified
            echo json_encode($result) . "<hr />";
        }
    }

    public function authorizedResource($id){


        $this->load->library('storage', array('id'=> $id));

        $storage = new Storage(array('id'=> $id));

        $xeroTenantId = (string)$storage->getSession()->tenant_id;

        if($storage->getSession()->access_token &&
            $storage->getSession()->tenant_id &&
            $storage->getSession()->refresh_token &&
            $storage->getSession()->expires &&
            $storage->getSession()->id_token
        ){
            //do nothing
        }
        else
        {
            return false;
        }




        if ($storage->getHasExpired()) {
            $provider = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => '415FACB4957F437EB19A980AE90FF239',
                'clientSecret'            => 'Quwq6OEMsKRw_XSAU7jku2ovlq39TOKhIQhAvqBtCmfa1cGJ',
                'redirectUri'             => 'https://evalu.ca/autosmart/index.php?auth/redirect_uri/',
                'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
                'urlAccessToken'          => 'https://identity.xero.com/connect/token',
                'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
            ]);

            $newAccessToken = $provider->getAccessToken('refresh_token', [
                'refresh_token' => $storage->getRefreshToken()
            ]);

            // Save my token, expiration and refresh token
            $storage->setToken(
                $newAccessToken->getToken(),
                $newAccessToken->getExpires(),
                $xeroTenantId,
                $newAccessToken->getRefreshToken()

            );
        }

        return $storage;
    }

    public function companies()
    {


        echo '<pre>';
        $franchises = $this->db->get_where('franchise', array('status'=>1))->result_array();


        if (count($franchises) > 0) {

            foreach ($franchises as $franchise)
            {

                $storage = null;
                try
                {
                    $storage = $this->authorizedResource($franchise['franchise_id']);

                }
                catch (Exception $e)
                {
                    print_r($e->getMessage());
                    print_r($franchise);
                }

                //  var_dump($storage);
                if(!$storage)
                {
                    continue;
                }
                $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                $config->setHost($this->host);

                $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                    new GuzzleHttp\Client(),
                    $config
                );

                // Get Organisation details
                $apiResponse = $apiInstance->getOrganisations((string)$storage->getSession()->tenant_id);
                // $c = $apiInstance->getContacts((string)$storage->getSession()->tenant_id);
                print_r($apiResponse);


            }
        }


    }

    public function get_pdf() {
// first get an invoice number to use
        $org_invoices = $this->xero->Invoices;
        $invoice_count = sizeof($org_invoices->Invoices->Invoice);
        $invoice_index = rand(0,$invoice_count);
        $invoice_id = (string) $org_invoices->Invoices->Invoice[$invoice_index]->InvoiceID;
        if(!$invoice_id) {
            echo "You will need some invoices for this...";
        }

// now retrieve that and display the pdf
        $pdf_invoice = $this->xero->Invoices($invoice_id, '', '', '', 'pdf');
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="the.pdf"');
        echo ($pdf_invoice);
    }

    public function update_contacts(){


        echo '<pre>';
        $franchises = $this->db->get_where('franchise', array('status'=>1))->result_array();


        if (count($franchises) > 0) {

            foreach ($franchises as $franchise)
            {

                //    print_r($franchise);
                $storage = null;
                try
                {
                    $storage = $this->authorizedResource($franchise['franchise_id']);

                }
                catch (Exception $e)
                {
                    print_r($e->getMessage());
                   print_r($franchise);
                }

              //  var_dump($storage);
                if(!$storage)
                {
                    continue;
                }

                $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                $config->setHost($this->host);

                $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                    new GuzzleHttp\Client(),
                    $config
                );

                // Get Organisation details
                //  $apiResponse = $apiInstance->getOrganisations((string)$storage->getSession()->tenant_id);
                try
                {
                    $contacts = $apiInstance->getContacts((string)$storage->getSession()->tenant_id);

                }
                catch (Exception $e)
                {
                    echo $e->getMessage();
                    continue;
                }




                if(count($contacts['contacts'])){

                    foreach ($contacts['contacts'] as $xero_contact)
                    {

                        $xero_contact = json_decode((string) $xero_contact);

                        if($xero_contact->ContactStatus == 'ACTIVE')
                        {
                           //Do Nothning
                        }
                        else
                        {
                            echo 'Contact Status '.$xero_contact->ContactStatus;
                            continue;
                        }
                        print_r($xero_contact);
						if($xero_contact->Addresses[1]->AddressLine1 == "" || is_null($xero_contact->Addresses[1]->AddressLine1)){
							$pobox_address_line_1 = "";
						}else{
							$pobox_address_line_1 = $xero_contact->Addresses[1]->AddressLine1;
						}
						if($xero_contact->EmailAddress == "" || is_null($xero_contact->EmailAddress)){
							$email = "";
						}else{
							$email = $xero_contact->EmailAddress;
						}
						if($xero_contact->Addresses[1]->City == "" || is_null($xero_contact->Addresses[1]->City)){
							$pobox_city = "";
						}else{
							$pobox_city = $xero_contact->Addresses[1]->City;
						}
						if($xero_contact->Addresses[1]->Region == "" || is_null($xero_contact->Addresses[1]->Region)){
							$pobox_region = "";
						}else{
							$pobox_region = $xero_contact->Addresses[1]->Region;
						}
						if($xero_contact->Addresses[1]->PostalCode == "" || is_null($xero_contact->Addresses[1]->PostalCode)){
							$pobox_postal_code = "";
						}else{
							$pobox_postal_code = $xero_contact->Addresses[1]->PostalCode;
						}
						if($xero_contact->Addresses[1]->Country == "" || is_null($xero_contact->Addresses[1]->Country)){
							$pobox_country = "";
						}else{
							$pobox_country = $xero_contact->Addresses[1]->Country;
						}
						if($xero_contact->Addresses[1]->AttentionTo == "" || is_null($xero_contact->Addresses[1]->AttentionTo)){
							$pobox_attention_to = "";
						}else{
							$pobox_attention_to = $xero_contact->Addresses[1]->AttentionTo;
						}
						
						if($xero_contact->Phones[1]->PhoneNumber == "" || is_null($xero_contact->Phones[1]->PhoneNumber)){
							$default_phone_number = "";
						}else{
							$default_phone_number = $xero_contact->Phones[1]->PhoneNumber;
						}
						if($xero_contact->Phones[1]->PhoneAreaCode == "" || is_null($xero_contact->Phones[1]->PhoneAreaCode)){
							$default_phone_area_code = "";
						}else{
							$default_phone_area_code = $xero_contact->Phones[1]->PhoneAreaCode;
						}
						if($xero_contact->Phones[1]->PhoneCountryCode == "" || is_null($xero_contact->Phones[1]->PhoneCountryCode)){
							$default_phone_country_code = "";
						}else{
							$default_phone_country_code = $xero_contact->Phones[1]->PhoneCountryCode;
						}
						
                        $data = array(
                            'xero_id' => $xero_contact->ContactID,
                            'franchise_id' => $franchise['franchise_id'],
                            'xero_number' => $xero_contact->ContactNumber,
                            'name' => $xero_contact->Name,
                            'email' => $email,
                            'pobox_address_line_1' => $pobox_address_line_1,
                            'pobox_city' => $pobox_city,
                            'pobox_region' => $pobox_region,
                            'pobox_postal_code' => $pobox_postal_code,
                            'pobox_country' => $pobox_country,
                            'pobox_attention_to' => $pobox_attention_to,
                            'street_address_line_1' => $xero_contact->Addresses[0]->AddressLine1,
                            'street_city' =>  $xero_contact->Addresses[0]->City,
                            'street_region' => $xero_contact->Addresses[0]->Region,
                            'street_postal_code' => $xero_contact->Addresses[0]->PostalCode,
                            'street_country' => $xero_contact->Addresses[0]->Country,
                            'street_attention_to' => $xero_contact->Addresses[0]->AttentionTo,
                            'ddi_phone_number' => $xero_contact->Phones[0]->PhoneNumber,
                            'ddi_phone_area_code' => $xero_contact->Phones[0]->PhoneAreaCode,
                            'ddi_phone_country_code' => $xero_contact->Phones[0]->PhoneCountryCode,
                            'default_phone_number' => $default_phone_number,
                            'default_phone_area_code' => $default_phone_area_code,
                            'default_phone_country_code' => $default_phone_country_code,
                            'fax_phone_number' => $xero_contact->Phones[2]->PhoneNumber,
                            'fax_phone_area_code' => $xero_contact->Phones[2]->PhoneAreaCode,
                            'fax_phone_country_code' =>$xero_contact->Phones[2]->PhoneCountryCode,
                            'tax_number' =>$xero_contact->TaxNumber,
                            'created_at' => date("Y-m-d"),
                            'updated_at' => date("Y-m-d"),
                        );
                        print_r($data);


                        //  var_dump($xero_contact->IsSupplier);

                        if($xero_contact->IsCustomer == '1'){
                            //   echo 'contact get '.$xero_contact->ContactID[0].'</br>';

                            $contact = $this->db->get_where('contacts', array('xero_id'=>$xero_contact->ContactID, 'franchise_id'=>$franchise['franchise_id']))->result_array();

                            if(count($contact) )
                            {

                                echo 'contact update '.$xero_contact->ContactID[0].'</br>';
                                $this->db->where('id', $contact[0]->id);
                                $this->db->update('contacts', $data);
                            }
                            else
                            {
                                //     print_r($data);
                                $this->db->insert('contacts', $data);
                                echo 'contact insert '.$xero_contact->ContactID[0].'</br>';

                            }
                        }
                    }



                }
                else
                {
                    echo 'No Contact';
                }


            }
        }


        // automatically instantiates the Xero class with your key, secret
        // and paths to your RSA cert and key
        // according to the configuration options you defined in appication/config/xero.php

    }

    public function push_to_xero($id = 'live'){



        echo '<pre>';

        if($id == 'dev')
        {
            echo 'dev';
            $franchises = $this->db->get_where('franchise', array('status'=>1, 'franchise_id'=>42))->result_array();
        print_r($franchises);
        
        }
        else
        {
            $franchises = $this->db->get_where('franchise', array('status'=>1,'integrate_with'=>'xero'))->result_array();

        }




        if (count($franchises) > 0) {

            foreach ($franchises as $franchise)
            {

                $storage = null;
                try
                {
                    $storage = $this->authorizedResource($franchise['franchise_id']);

                }
                catch (Exception $e)
                {
                    print_r($e->getMessage());
                    print_r($franchise);
                }


                if(!$storage)
                {
                    continue;
                }


                $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                $config->setHost($this->host);

                $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                    new GuzzleHttp\Client(),
                    $config
                );

                // Get Organisation details
                //  $apiResponse = $apiInstance->getOrganisations((string)$storage->getSession()->tenant_id);

                $query = "SELECT * FROM contacts  WHERE contacts.status = 1  AND contacts.isSynced = 0   AND contacts.franchise_id = ".$franchise['franchise_id']."";

                $contacts = $this->db->query($query)->result_array();
                // echo count($contacts);

                if(count($contacts)){

                    foreach ($contacts as $xero_contact)
                    {
                        print_r($xero_contact);
                        // echo $xero_contact;
                        $xero_contact = json_decode(json_encode($xero_contact));
                        $phoneDefault =   array(
                            "PhoneType" => "DEFAULT",
                            "PhoneNumber" => $xero_contact->default_phone_number,
                            "PhoneAreaCode" => $xero_contact->default_phone_area_code,
                            "PhoneCountryCode" => $xero_contact->default_phone_country_code
                        );

                        $phoneFax =   array(
                            "PhoneType" => "FAX",
                            "PhoneNumber" => $xero_contact->fax_phone_number,
                            "PhoneAreaCode" => $xero_contact->fax_phone_area_code,
                            "PhoneCountryCode" => $xero_contact->fax_phone_country_code
                        );

                        $phoneDdi =   array(
                            "PhoneType" => "DDI",
                            "PhoneNumber" => $xero_contact->ddi_phone_number,
                            "PhoneAreaCode" => $xero_contact->ddi_phone_area_code,
                            "PhoneCountryCode" => $xero_contact->ddi_phone_country_code
                        );
                        $addressPOBOX = array(
                            "AddressType" => "POBOX",
                            "AddressLine1" => $xero_contact->pobox_address_line_1,
                            "City" => $xero_contact->pobox_city,
                            "PostalCode" => $xero_contact->pobox_postal_code,
                            "Country" => $xero_contact->pobox_country,
                            "AttentionTo" => $xero_contact->pobox_attention_to,
                            "Region" => $xero_contact->pobox_region
                        );

                        $addressStreet = array(
                            "AddressType" => "STREET",
                            "AddressLine1" => $xero_contact->street_address_line_1,
                            "City" => $xero_contact->street_city,
                            "PostalCode" => $xero_contact->street_postal_code,
                            "Country" => $xero_contact->street_country,
                            "AttentionTo" => $xero_contact->street_attention_to,
                            "Region" => $xero_contact->street_region
                        );

                        $new_contact['Name'] = $xero_contact->name;
                        $new_contact['EmailAddress'] = $xero_contact->email;
                        $new_contact['Addresses'] = array($addressPOBOX,$addressStreet );

                        $new_contact['Phones'] = array($phoneDefault, $phoneFax, $phoneDdi );
                       /* $new_contact['Addresses'][1] = $addressPOBOX;
                        $new_contact['Addresses'][0] = $addressStreet;*/
                     /*   $new_contact['Phones'][0] = $phoneDefault;
                        $new_contact['Phones'][1] = $phoneFax;
                        $new_contact['Phones'][2] = $phoneDdi;*/

                        if($xero_contact->xero_id){
                            $new_contact['ContactID'] = $xero_contact->xero_id;
                        }

                        print_r($new_contact);
                        echo json_encode($new_contact);
                        echo '=====';
                        $xero_tenant_id = $storage->getSession()->tenant_id; // string | Xero identifier for Tenant

                        try {
                            $result = $apiInstance->createContacts($xero_tenant_id, $new_contact);

                            print_r($result);
                            if(isset($result['contacts'][0]))
                            {
                                $contact = $result['contacts'][0];

                                $update_id = "SELECT * FROM contacts  WHERE contacts.id = ".$xero_contact->id."";
                                $update_id = $this->db->query($update_id)->result_array();

                                if(count($update_id)) {

                                    $data = array(
                                        'xero_id' => $contact['contact_id'],
                                        'isSynced' => 1
                                    );

                                    print_r($contact);
                                    print_r($data);

                                    $update_query = "UPDATE `contacts` SET `xero_id` = '".$contact['contact_id']."' WHERE `contacts`.`id` = ".$update_id[0]['id']."";
                                    $update_query = $this->db->query($update_query);

                                   $update_query = "UPDATE `contacts` SET `isSynced` = '1' WHERE `contacts`.`id` = ".$update_id[0]['id']."";
                                   $update_query = $this->db->query($update_query);

                                }
                            }
                        } catch (Exception $e) {
                            echo 'Exception when calling AccountingApi->createContacts: ', $e->getMessage(), PHP_EOL;
                        }





                    }



                }


            }
        }



    }

    public function push_invoices($timezone = ""){
        
        echo '<pre>';
        $franchises = $this->db->get_where('franchise', array('status'=>1,'integrate_with'=>'xero','time_zone'=>$timezone))->result_array();
print_r($franchises);
        if (count($franchises) > 0) {

            foreach ($franchises as $franchise)
            {

                $storage = null;
                try
                {
                    $storage = $this->authorizedResource($franchise['franchise_id']);

                }
                catch (Exception $e)
                {
                    print_r($e->getMessage());
                    print_r($franchise);
                    continue;
                }

                if(!$storage)
                {
                    continue;
                }
                $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                $config->setHost($this->host);

                $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                    new GuzzleHttp\Client(),
                    $config
                );


                $query = 'SELECT 
                          i.id,
                          i.created_at,
                          i.date,
                          i.due_date,
                          i.reference_number,
                          i.surcharge_price,
                          i.invoice_number,
                          i.total_price,
                          c.name as contact_name, 
                          f.name as franchise_name, 
                          c.email as contact_email 
                          FROM invoices i 
                          LEFT JOIN contacts c ON c.id = i.contact_id 
                          LEFT JOIN franchise f on f.franchise_id = i.franchise_id 
                          WHERE i.status = 1 and i.xero_id IS NULL and i.franchise_id = '.$franchise['franchise_id'].'';
                $invoices = $this->db->query($query)->result_array();

echo count($invoices);
                if(count($invoices)){

                    foreach ($invoices as $invoice)
                    {


                        print_r($invoice);

                        $query = 'SELECT p.name, i.* from line_items i 
                left  join products p on p.products_id = i.product_id
                where i.status = 1 and i.invoice_id = '.$invoice['id'].'';

                        $items = $this->db->query($query)->result_array();
                        $line_items = array();
						$item_count = 0;
                        $total_line_price = 0;
                        $total_invoice_price = 0;

                        foreach ($items as $key => $item)
                        {
                            $total_line_price = $total_line_price + $item['size'];

                            if($item['discount_type'] == "percentage"){
                                $line_items[$key] =  array(
                                    "Description" => ($item['name'] == '' ?  '--' : $item['name']),
                                    "Quantity" => $item['quantity'],
                                    "UnitAmount" => $item['price'],
                                    "DiscountRate" => $item['discount_value'],
                                    "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                );
                            }else if($item['discount_type'] == "amount"){
                                $line_items[$key] =  array(
                                    "Description" => ($item['name'] == '' ?  '--' : $item['name']),
                                    "Quantity" => $item['quantity'],
                                    "UnitAmount" => $item['price'],
                                    "DiscountAmount" => $item['discount_value'],
                                    "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                );
                            }else{
                                $line_items[$key] =  array(
                                    "Description" => ($item['name'] == '' ?  '--' : $item['name']),
                                    "Quantity" => $item['quantity'],
                                    "UnitAmount" => $item['price'],
                                    "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                );
                            }
                            
							$item_count++;
                        }
						
						if($invoice['surcharge_price'] != "0.00"){
                            $line_items[$item_count] =  array(
                                "Description" => "Surcharge",
                                "Quantity" => "1",
                                "UnitAmount" => $invoice['surcharge_price'],
								"AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                            );
						}

                        print_r($invoice);

                        $new_invoice =
                            array(
                                "Type"=> 'ACCREC',
                                "Contact" => array(
                                    "Name" =>str_replace("'", "\'", $invoice['contact_name']) 
                                ),
                                "Date" => date('Y-m-d', strtotime($invoice['date']) ),
                                "DueDate" => $invoice['due_date'],
                                "Status" => "AUTHORISED",
                                "LineAmountTypes" => "Exclusive",
                                "Reference" => $invoice['reference_number'],
                                "InvoiceNumber" => $invoice['invoice_number'],
                                "LineItems"=>  $line_items,
								"TotalTax"=>  $invoice['surcharge_price'],


                            );
                            $total_invoice_price = $invoice['total_price']-($invoice['total_price']*0.10)-$invoice['surcharge_price'];

                        if($invoice['reference_number'] == '' || $invoice['reference_number'] == null )
                        {
                             unset($new_invoice['reference_number']);
                        }


                        print_r($new_invoice);




                        $xero_tenant_id = $storage->getSession()->tenant_id; // string | Xero identifier for Tenant

                        try {


                            if($total_invoice_price == $total_line_price){
                                $result = $apiInstance->createInvoice($xero_tenant_id, $new_invoice);
                            }



                            if(isset($result['invoices'][0]['invoice_id']))
                            {
                                $update_query = "UPDATE `invoices` SET `xero_id` = '".$result['invoices'][0]['invoice_id']."', `status` = '2' WHERE `invoices`.`id` = ".$invoice['id']."";
                                $update_query = $this->db->query($update_query);
                                $invoice_email = $apiInstance->emailInvoice($xero_tenant_id, $result['invoices'][0]['invoice_id'], true);
                                print_r($invoice_email);
                                echo 'update';
                            }
                        } catch (Exception $e) {
                            print_r($e);
                            echo 'Exception when calling Invoice Api: ', $e->getMessage(), PHP_EOL;
                        }


                        // $invoice_result = $instance->Invoices($new_invoice);



                    }



                }

            }
        }
    }

    /**
     * @return string
     */

    public function renderJSON($jsonData) {
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($jsonData));
    }

    public function push_single_invoice(){
        //die();
        $response['status'] = 'Error';
        $response['code'] = 404;
        $response['records'] = array();
        $response['messageContent'] = 'Please Provide Invoice ID.';

        //$this->_curl(base_url().'index.php?xero_services/push_to_xero');

        $franchise_id = $this->input->post('franchise_id');
        $invoice_id = $this->input->post('invoice_id');
        
        if($invoice_id != "" && $franchise_id != ""){
            
            $franchises = $this->db->get_where('franchise', array('status'=>1, 'franchise_id'=>$franchise_id))->result_array();
            if (count($franchises) > 0) {

                foreach ($franchises as $franchise)
                {

                    $storage = null;
                    try
                    {
                        $storage = $this->authorizedResource($franchise['franchise_id']);

                    }
                    catch (Exception $e)
                    {
                        //print_r($e->getMessage());
                        //print_r($franchise);
                        continue;
                    }
                   if(!$storage)
                    {
                        continue;
                    }
                    $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                    $config->setHost($this->host);
                    $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                        new GuzzleHttp\Client(),
                        $config
                    );


                    $query = 'SELECT 
                            i.id,
                            i.created_at,
                            i.date,
                            i.due_date,
                            i.reference_number,
                            i.surcharge_price,
                            i.invoice_number,
                            c.name as contact_name, 
                            f.name as franchise_name, 
                            c.email as contact_email 
                            FROM invoices i 
                            LEFT JOIN contacts c ON c.id = i.contact_id 
                            LEFT JOIN franchise f on f.franchise_id = i.franchise_id 
                            WHERE i.status = 1 and i.xero_id IS NULL and i.id = '.$invoice_id.'';
                    $invoices = $this->db->query($query)->result_array();

                    if(count($invoices)){

                        foreach ($invoices as $invoice)
                        {


                            //print_r($invoice);

                            $query = 'SELECT p.name, i.* from line_items i 
                    left  join products p on p.products_id = i.product_id
                    where i.status = 1 and i.invoice_id = '.$invoice['id'].'';

                            $items = $this->db->query($query)->result_array();
                            $line_items = array();
                            $item_count = 0;
                            foreach ($items as $key => $item)
                            {
                                if($item['discount_type'] == "percentage"){
                                    $line_items[$key] =  array(
                                        "Description" => $item['name'],
                                        "Quantity" => $item['quantity'],
                                        "UnitAmount" => $item['price'],
                                        "DiscountRate" => $item['discount_value'],
                                        "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                    );
                                }else if($item['discount_type'] == "amount"){
                                    $line_items[$key] =  array(
                                        "Description" => $item['name'],
                                        "Quantity" => $item['quantity'],
                                        "UnitAmount" => $item['price'],
                                        "DiscountAmount" => $item['discount_value'],
                                        "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                    );
                                }else{
                                    $line_items[$key] =  array(
                                        "Description" => $item['name'],
                                        "Quantity" => $item['quantity'],
                                        "UnitAmount" => $item['price'],
                                        "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                    );
                                }
                                
                                $item_count++;
                            }
                            
                            if($invoice['surcharge_price'] != "0.00"){
                                $line_items[$item_count] =  array(
                                    "Description" => "Surcharge",
                                    "Quantity" => "1",
                                    "UnitAmount" => $invoice['surcharge_price'],
                                    "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                );
                            }

                            //print_r($invoice);

                            $new_invoice =
                                array(
                                    "Type"=> 'ACCREC',
                                    "Contact" => array(
                                        "Name" => addslashes($invoice['contact_name'])
                                    ),
                                    "Date" => date('Y-m-d', strtotime($invoice['created_at']) ),
                                    "DueDate" => $invoice['due_date'],
                                    "Status" => "AUTHORISED",
                                    "LineAmountTypes" => "Exclusive",
                                    "Reference" => $invoice['reference_number'],
                                    "InvoiceNumber" => $invoice['invoice_number'],
                                    "LineItems"=>  $line_items,
                                    "TotalTax"=>  $invoice['surcharge_price'],


                                );

                            if($invoice['reference_number'] == '' || $invoice['reference_number'] == null )
                            {
                                unset($new_invoice['reference_number']);
                            }


                            //print_r($new_invoice);die();



                            $xero_tenant_id = $storage->getSession()->tenant_id; // string | Xero identifier for Tenant

                            try {



                                $result = $apiInstance->createInvoice($xero_tenant_id, $new_invoice);



                                if(isset($result['invoices'][0]['invoice_id']))
                                {
                                    $update_query = "UPDATE `invoices` SET `xero_id` = '".$result['invoices'][0]['invoice_id']."', `status` = '2' WHERE `invoices`.`id` = ".$invoice['id']."";
                                    $update_query = $this->db->query($update_query);
                                    try{
                                    $invoice_email = $apiInstance->emailInvoice($xero_tenant_id, $result['invoices'][0]['invoice_id'], true);
                                    }
                                    catch (Exception $e) {
                                        $response['status'] = 'Success';
                                        $response['code'] = 200;
                                        $response['records'] = array();
                                        $response['messageContent'] = 'Synced.';
                                    }
                                    //print_r($invoice_email);
                                    //echo 'update';
                                    $response['status'] = 'Success';
                                    $response['code'] = 200;
                                    $response['records'] = array();
                                    $response['messageContent'] = 'Synced.';
                                }
                                else{
                                    $response['status'] = 'Success';
                                    $response['code'] = 201;
                                    $response['records'] = array();
                                    $response['messageContent'] = 'Sync failed.';
                                }
                            } catch (Exception $e) {
                                //print_r($e);
                                //echo 'Exception when calling Invoice Api: ', $e->getMessage(), PHP_EOL;
                                $response['status'] = 'Success';
                                $response['code'] = 404;
                                $response['records'] = array();
                                $response['messageContent'] = 'Exception : '.$e->getMessage();
                            }


                            // $invoice_result = $instance->Invoices($new_invoice);



                        }



                    }
                    else{
                        $response['status'] = 'Error';
                        $response['code'] = 404;
                        $response['records'] = array();
                        $response['messageContent'] = 'No Valid Invoice Found.';
                    }

                }
            }
            else{
                $response['status'] = 'Error';
                $response['code'] = 404;
                $response['records'] = array();
                $response['messageContent'] = 'No Valid Franchise Found.';
            }
        } else
        {
            $response['status'] = 'Error';
            $response['code'] = 404;
            $response['records'] = array();
            $response['messageContent'] = 'No Valid Invoice Found.';
        }  
//        print_r($response);
        $this->renderJSON($response);
    }

    public function email_invoices($id = null)
    {

        $response['status'] = 'Error';
        $response['code'] = 404;
        $response['records'] = array();
        $response['messageContent'] = 'Please Provide Invoice ID.';
        if($id)
        {

            $query = 'SELECT 
                          i.id,
                          i.created_at,
                          i.xero_id,
                          i.date,
                          i.due_date,
                          f.franchise_id as franchise_id,
                          c.name as contact_name, 
                          f.name as franchise_name, 
                          c.email as contact_email 
                          FROM invoices i 
                          LEFT JOIN contacts c ON c.id = i.contact_id 
                          LEFT JOIN franchise f on f.franchise_id = i.franchise_id 
                          WHERE i.status = 2 and i.id = "'.$id.'" ';
						  
            $invoices = $this->db->query($query)->result_array();


            if(count($invoices)){


                $franchises = $this->db->get_where('franchise', array('status'=>1, 'franchise_id' => $invoices[0]['franchise_id']))->result_array();
                if (count($franchises) > 0) {



                    foreach ($franchises as $franchise)
                    {

                        $storage = null;
                        try
                        {
                            $storage = $this->authorizedResource($franchise['franchise_id']);

                        }
                        catch (Exception $e)
                        {
                            print_r($e->getMessage());
                            print_r($franchise);
                        }

                       

                        $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                        $config->setHost($this->host);

                        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                            new GuzzleHttp\Client(),
                            $config
                        );



                        $invoice = $invoices[0];
                        $xero_tenant_id = $storage->getSession()->tenant_id; // string | Xero identifier for Tenant

                        print_r($invoice);
                        print_r($xero_tenant_id);
                        try {

                            if(isset($invoice['xero_id']))
                            {
								$invoice_email = $apiInstance->emailInvoice($xero_tenant_id, $invoice['xero_id'], true);
                                $response['status'] = 'Success';
                                $response['code'] = 200;
                                $response['records'] = array();
                                $response['messageContent'] = 'Email Sent.';
                            }
                            else
                            {
                                $response['messageContent'] = 'Invalid Invoice.';
                            }
                        } catch (Exception $e) {

                            $response['messageContent'] = 'Exception when calling Email Api: '.$e->getMessage();

                        }

                    }

                }

            }else
            {
                $response['status'] = 'Error';
                $response['code'] = 404;
                $response['records'] = array();
                $response['messageContent'] = 'No Valid Invoice Found.';
            }

        }



        $this->renderJSON($response);

    }

    public function  all_crons()
    {


        ini_set('max_execution_time', 0);

        $response['message'] = 'An Error Occure Please Try Again Later';
        $response['status'] = '1';
        
        $inprocess = $this->get_all_crons_process();

        if($this->check_inprocess($inprocess, 10 ))
        {
            $response['message'] = 'Services already in process';
            $response['Error'] = '1';
            echo json_encode($response);
            die();
        }
        else
        {
            $this->update_all_process(time( 'timestamp', 0 ));

            echo $this->_curl(base_url().'index.php?xero_services/push_to_xero');
            
            $this->update_all_process(time( 'timestamp', 0 ));
            echo $this->_curl(base_url().'index.php?xero_services/push_invoices');
            $this->update_all_process(0);
            echo "salman";

        }
    }

   private function check_inprocess($time, $diff)
    {

        if($time == 0){
            return false;
        }
        $to_time = time();
        $from_time = $time;
        $cron_runing_diff = round(abs($to_time - $from_time) / 60,0). "";

        if($cron_runing_diff<$diff)
        {
            return true;
        }else
        {
            return false;
        }
    }

    private function get_all_crons_process()
    {
        $content = @file_get_contents('in_process.txt');
        if($content === FALSE) {
            return false;
        }
        else
        {
            return $content;
        }

    }

    private function  update_all_process($process = 0)
    {
        $WriteVid = file_put_contents('in_process.txt', $process);
    }
    private function _curl($file){

        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL,$file);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, '');
        curl_setopt( $curl_handle, CURLOPT_CONNECTTIMEOUT, 43200 );
        curl_setopt( $curl_handle, CURLOPT_TIMEOUT, 43200 );
        $query = curl_exec($curl_handle);

        curl_close($curl_handle);
        return $query;
    }


    public function update_contacts_single($id){


        echo '<pre>';
        $franchises = $this->db->get_where('franchise', array('franchise_id'=>$id))->result_array();


        if (count($franchises) > 0) {

            foreach ($franchises as $franchise)
            {

                //    print_r($franchise);
                $storage = null;
                try
                {
                    $storage = $this->authorizedResource($franchise['franchise_id']);

                }
                catch (Exception $e)
                {
                    print_r($e->getMessage());
                    print_r($franchise);
                }

                //  var_dump($storage);
                if(!$storage)
                {
                    continue;
                }

                $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                $config->setHost($this->host);

                $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                    new GuzzleHttp\Client(),
                    $config
                );

                // Get Organisation details
                //  $apiResponse = $apiInstance->getOrganisations((string)$storage->getSession()->tenant_id);
                try
                {
                    $contacts = $apiInstance->getContacts((string)$storage->getSession()->tenant_id);

                }
                catch (Exception $e)
                {
                    echo $e->getMessage();
                    continue;
                }




                if(count($contacts['contacts'])){

                    foreach ($contacts['contacts'] as $xero_contact)
                    {

                        $xero_contact = json_decode((string) $xero_contact);



                        if($xero_contact->ContactStatus == 'ACTIVE')
                        {
                            //Do Nothning
                        }
                        else
                        {
                            continue;
                        }

                        print_r($xero_contact);
                        $data = array(
                            'xero_id' => $xero_contact->ContactID,
                            'franchise_id' => $franchise['franchise_id'],
                            'xero_number' => $xero_contact->ContactNumber,
                            'name' => $xero_contact->Name,
                            'email' => $xero_contact->EmailAddress,
                            'pobox_address_line_1' => $xero_contact->Addresses[0]->AddressLine1,
                            'pobox_city' => $xero_contact->Addresses[0]->City,
                            'pobox_region' => $xero_contact->Addresses[0]->Region,
                            'pobox_postal_code' => $xero_contact->Addresses[0]->PostalCode,
                            'pobox_country' => $xero_contact->Addresses[0]->Country,
                            'pobox_attention_to' => $xero_contact->Addresses[0]->AttentionTo,
                            'street_address_line_1' => $xero_contact->Addresses[1]->AddressLine1,
                            'street_city' =>  $xero_contact->Addresses[1]->City,
                            'street_region' => $xero_contact->Addresses[1]->Region,
                            'street_postal_code' => $xero_contact->Addresses[1]->PostalCode,
                            'street_country' => $xero_contact->Addresses[1]->Country,
                            'street_attention_to' => $xero_contact->Addresses[1]->AttentionTo,
                            'ddi_phone_number' => $xero_contact->Phones[0]->PhoneNumber,
                            'ddi_phone_area_code' => $xero_contact->Phones[0]->PhoneAreaCode,
                            'ddi_phone_country_code' => $xero_contact->Phones[0]->PhoneCountryCode,
                            'default_phone_number' => $xero_contact->Phones[1]->PhoneNumber,
                            'default_phone_area_code' => $xero_contact->Phones[1]->PhoneAreaCode,
                            'default_phone_country_code' => $xero_contact->Phones[1]->PhoneCountryCode,
                            'fax_phone_number' => $xero_contact->Phones[2]->PhoneNumber,
                            'fax_phone_area_code' => $xero_contact->Phones[2]->PhoneAreaCode,
                            'fax_phone_country_code' =>$xero_contact->Phones[2]->PhoneCountryCode,
                            'tax_number' =>$xero_contact->TaxNumber,
                            'updated_at' => date("Y-m-d"),
                        );
                        print_r($data);


                        //  var_dump($xero_contact->IsSupplier);

                        if($xero_contact->IsCustomer == '1'){
                            //   echo 'contact get '.$xero_contact->ContactID[0].'</br>';

                            $contact = $this->db->get_where('contacts', array('xero_id'=>$xero_contact->ContactID, 'franchise_id'=>$franchise['franchise_id']))->result_array();

                            if(count($contact) )
                            {

                                echo 'contact update '.$xero_contact->ContactID[0].'</br>';
                                $this->db->where('id', $contact[0]->id);
                                $this->db->update('contacts', $data);
                            }
                            else
                            {
                                //     print_r($data);
                                $this->db->insert('contacts', $data);
                                echo 'contact insert '.$xero_contact->ContactID[0].'</br>';

                            }

                            $update_query_new = "UPDATE contacts SET email = '' WHERE contacts.email is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET pobox_address_line_1 = '' WHERE contacts.pobox_address_line_1 is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET pobox_city = '' WHERE contacts.pobox_city is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET pobox_region = '' WHERE contacts.pobox_region is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET pobox_postal_code = '' WHERE contacts.pobox_postal_code is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET pobox_country = '' WHERE contacts.pobox_country is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET default_phone_number = '' WHERE contacts.default_phone_number is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET default_phone_area_code = '' WHERE contacts.default_phone_area_code is NULL";
                            $update_query = $this->db->query($update_query_new);

                            $update_query_new = "UPDATE contacts SET default_phone_country_code = '' WHERE contacts.default_phone_country_code is NULL";
                            $update_query = $this->db->query($update_query_new);
                        }
                    }



                }
                else
                {
                    echo 'No Contact';
                }


            }
        }


        // automatically instantiates the Xero class with your key, secret
        // and paths to your RSA cert and key
        // according to the configuration options you defined in appication/config/xero.php

    }


    public function update_single_invoice(){



        $response['status'] = 'Error';
        $response['code'] = 404;
        $response['records'] = array();
        $response['messageContent'] = 'Please Provide Invoice ID.';

        $franchise_id = $this->input->post('franchise_id');
        $invoice_id = $this->input->post('invoice_id');
        echo '<pre>';
//echo $franchise_id;
//echo $invoice_id;
        if($invoice_id != "" && $franchise_id != ""){

            $franchises = $this->db->get_where('franchise', array('status'=>1, 'franchise_id'=>$franchise_id))->result_array();

         //print_r($franchises);
            if (count($franchises) > 0) {

                foreach ($franchises as $franchise)
                {

                    $storage = null;
                    try
                    {
                        $storage = $this->authorizedResource($franchise['franchise_id']);

                    }
                    catch (Exception $e)
                    {
                        //print_r($e->getMessage());
                        //print_r($franchise);
                        continue;
                    }

                    if(!$storage)
                    {
                        continue;
                    }
                    $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                    $config->setHost($this->host);

                    $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                        new GuzzleHttp\Client(),
                        $config
                    );


                    $query = 'SELECT 
                            i.id,
                            i.created_at,
                            i.date,
                            i.due_date,
                            i.xero_id,
                            i.reference_number,
                            i.surcharge_price,
                            i.invoice_number,
                            c.name as contact_name, 
                            f.name as franchise_name, 
                            c.email as contact_email 
                            FROM invoices i 
                            LEFT JOIN contacts c ON c.id = i.contact_id 
                            LEFT JOIN franchise f on f.franchise_id = i.franchise_id 
                            WHERE  i.id = '.$invoice_id.'';
                    $invoices = $this->db->query($query)->result_array();

                    //print_r($invoices);
                    if(isset($invoices[0]['xero_id'])){


                        $xero_invoice = $apiInstance->getInvoice((string)$storage->getSession()->tenant_id, $invoices[0]['xero_id']);
                        print_r($xero_invoice);
                        die('xero invoice');

                    }


                    die('error');

                }
            }
        } else
        {
            $response['status'] = 'Error';
            $response['code'] = 404;
            $response['records'] = array();
            $response['messageContent'] = 'No Valid Invoice Found.';
        }

        $this->renderJSON($response);
    }
    public function xero_update_single_invoice(){



        $response['status'] = 'Error';
        $response['code'] = 404;
        $response['records'] = array();
        $response['messageContent'] = 'Please Provide Invoice ID.';

        $franchise_id = $this->input->post('franchise_id');
        $invoice_id = $this->input->post('invoice_id');

        if($invoice_id != "" && $franchise_id != ""){

            $franchises = $this->db->get_where('franchise', array('status'=>1, 'franchise_id'=>$franchise_id))->result_array();
            if (count($franchises) > 0) {

                foreach ($franchises as $franchise)
                {

                    $storage = null;
                    try
                    {
                        $storage = $this->authorizedResource($franchise['franchise_id']);

                    }
                    catch (Exception $e)
                    {
                        //print_r($e->getMessage());
                        //print_r($franchise);
                        continue;
                    }
                    if(!$storage)
                    {
                        continue;
                    }
                    $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()->access_token);
                    $config->setHost($this->host);
                    $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                        new GuzzleHttp\Client(),
                        $config
                    );


                    $query = 'SELECT 
                            i.id,
                            i.created_at,
                            i.date,
                            i.due_date,
                            i.xero_id,
                            i.reference_number,
                            i.surcharge_price,
                            i.invoice_number,
                            c.name as contact_name, 
                            f.name as franchise_name, 
                            c.email as contact_email 
                            FROM invoices i 
                            LEFT JOIN contacts c ON c.id = i.contact_id 
                            LEFT JOIN franchise f on f.franchise_id = i.franchise_id 
                            WHERE i.id = '.$invoice_id.'';
                    $invoices = $this->db->query($query)->result_array();

                    if(count($invoices)){

                        foreach ($invoices as $invoice)
                        {

                            if($invoice['xero_id'])
                            {

                                //print_r($invoice);

                                $query = 'SELECT p.name, i.* from line_items i 
                    left  join products p on p.products_id = i.product_id
                    where i.status = 1 and i.invoice_id = '.$invoice['id'].'';

                                $items = $this->db->query($query)->result_array();
                                $line_items = array();
                                $item_count = 0;
                                foreach ($items as $key => $item)
                                {

                                    $item_query = 'SELECT i.line_item_id from line_item_xero i 
                                        where i.item_id =  '.$item['id'].'';

                                    $line_item_id = $this->db->query($item_query)->result_array();

                                    if(true)
                                    {




                                     //   $line_items[$key]['thisiskey'] = $line_item_id[0]['line_item_id'];

                                    if($item['discount_type'] == "percentage"){
                                        $line_items[$key] =  array(
                                            //"LineItemID" => $line_item_id[0]['line_item_id'],
                                            "Description" => $item['name'],
                                            "Quantity" => $item['quantity'],
                                            "UnitAmount" => $item['price'],
                                            "DiscountRate" => $item['discount_value'],
                                            "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                        );
                                    }else if($item['discount_type'] == "amount"){
                                        $line_items[$key] =  array(
                                            //"LineItemID" => $line_item_id[0]['line_item_id'],
                                            "Description" => $item['name'],
                                            "Quantity" => $item['quantity'],
                                            "UnitAmount" => $item['price'],
                                            "DiscountAmount" => $item['discount_value'],
                                            "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                        );
                                    }else{
                                        $line_items[$key] =  array(
                                            //"LineItemID" => $line_item_id[0]['line_item_id'],
                                            "Description" => $item['name'],
                                            "Quantity" => $item['quantity'],
                                            "UnitAmount" => $item['price'],
                                            "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                        );
                                    }


                                    }
                                    $item_count++;
                                }

                                if($invoice['surcharge_price'] != "0.00"){
                                    $line_items[$item_count] =  array(
                                        "Description" => "Surcharge",
                                        "Quantity" => "1",
                                        "UnitAmount" => $invoice['surcharge_price'],
                                        "AccountCode" => ($franchise['account_code'] ? $franchise['account_code'] : '200')
                                    );
                                }

                                //print_r($invoice);

                                $update_invoice =
                                    array(
                                        "Type"=> 'ACCREC',
                                        "Contact" => array(
                                            "Name" => addslashes($invoice['contact_name'])
                                        ),
                                        "Date" => date('Y-m-d', strtotime($invoice['date']) ),
                                        "DueDate" => $invoice['due_date'],
                                        "Status" => "AUTHORISED",
                                        "LineAmountTypes" => "Exclusive",
                                        "Reference" => $invoice['reference_number'],
                                        "InvoiceNumber" => $invoice['invoice_number'],
                                        "LineItems"=>  $line_items,
                                        "TotalTax"=>  $invoice['surcharge_price'],


                                    );

                                if($invoice['reference_number'] == '' || $invoice['reference_number'] == null )
                                {
                                    unset($update_invoice['reference_number']);
                                }


                                print_r($update_invoice);




                                $xero_tenant_id = $storage->getSession()->tenant_id; // string | Xero identifier for Tenant

                                try {



                                    $result = $apiInstance->updateInvoice($xero_tenant_id,$invoice['xero_id'], $update_invoice);

                                    //print_r($result);

                                    $response['status'] = 'Success';
                                    $response['code'] = 200;
                                    $response['records'] = array();
                                    $response['messageContent'] = '';


                                } catch (Exception $e) {

                                    print_r($e);
                                    //print_r($e);
                                    //echo 'Exception when calling Invoice Api: ', $e->getMessage(), PHP_EOL;
                                    $response['status'] = 'Error';
                                    $response['code'] = 404;
                                    $response['records'] = array();
                                    $response['messageContent'] = 'Exception when calling Email Api: '.$e->getMessage();
                                }


                                // $invoice_result = $instance->Invoices($new_invoice);




                            }


                        }



                    }

                }
            }
        } else
        {
            $response['status'] = 'Error';
            $response['code'] = 404;
            $response['records'] = array();
            $response['messageContent'] = 'No Valid Invoice Found.';
        }
//        print_r($response);
        $this->renderJSON($response);
    }


}