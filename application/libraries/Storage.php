<?php
class Storage
{
    public $franchise = null;

    function __construct($param) {

            $this->init_session($param['id']);
    }

    public function init_session($id){

        $CI =   &get_instance();

        $credential = array('franchise_id' => $id, 'status' => '1');

        $query = $CI->db->get_where('franchise', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->franchise = $row;
        }

    }

    public function getSession() {
        return $this->franchise;
    }

    public function startSession($token, $secret, $expires = null)
    {
        session_start();
    }

    public function setToken($token, $expires = null, $tenantId, $refreshToken, $idToken = null)
    {
        $CI =   &get_instance();
     /*   $_SESSION['oauth2'] = [
            'token' => $token,
            'expires' => $expires,
            'tenant_id' => $tenantId,
            'refresh_token' => $refreshToken,
            'id_token' => $idToken
        ];*/

        $update_query = "UPDATE `franchise` SET `access_token` = '".$token."' WHERE `franchise`.`franchise_id` = ".$this->franchise->franchise_id."";
        $update_query =  $CI->db->query($update_query);

        $update_query = "UPDATE `franchise` SET `tenant_id` = '".$tenantId."' WHERE `franchise`.`franchise_id` = ".$this->franchise->franchise_id."";
        $update_query =  $CI->db->query($update_query);

        $update_query = "UPDATE `franchise` SET `refresh_token` = '".$refreshToken."' WHERE `franchise`.`franchise_id` = ".$this->franchise->franchise_id."";
        $update_query =  $CI->db->query($update_query);

        $update_query = "UPDATE `franchise` SET `expires` = '".$expires."' WHERE `franchise`.`franchise_id` = ".$this->franchise->franchise_id."";
        $update_query =  $CI->db->query($update_query);

        if($idToken)
        {
            $update_query = "UPDATE `franchise` SET `id_token` = '".$idToken."' WHERE `franchise`.`franchise_id` = ".$this->franchise->franchise_id."";
            $update_query =  $CI->db->query($update_query);

        }

        $credential = array('franchise_id' => $this->franchise->franchise_id, 'status' => '1');

        $query =  $CI->db->get_where('franchise', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->franchise = $row;
        }

    }

    public function getToken()
    {
        //If it doesn't exist or is expired, return null
        if (!empty($this->getSession())
            || ($this->franchise->expires !== null
                && $this->franchise->expires <= time())
        ) {
            return null;
        }
        return $this->getSession();
    }

    public function getAccessToken()
    {
        return $this->franchise->access_token;
    }

    public function getRefreshToken()
    {
        return $this->franchise->refresh_token;
    }

    public function getExpires()
    {
        return $this->franchise->expires;
    }

    public function getXeroTenantId()
    {
        return $this->franchise->tenant_id;
    }

    public function getIdToken()
    {
        return $this->franchise->id_token;
    }

    public function getHasExpired()
    {
        if (!empty($this->getSession()))
        {
            if(time() > $this->getExpires())
            {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
?>