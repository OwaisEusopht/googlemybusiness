<?php 
class asses_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }
  public function insert($table,$data)
  {
    $this->db->insert($table, $data);
  }

  public function get_assessment_completed_detail($id){
        $sql = "SELECT * FROM assessment where idassessment = '$id' ";
        $query = $this->db->query($sql);
        return $query->result();
  }

  public function get_data($id,$aid) {
   $sql="SELECT * from assessment_questions where question_categoryid= '$id' and assessment_id= '$aid' ";
   $query = $this->db->query($sql);
   return $query->result();
 }
 public function insert_query($table, $data) {
  $query = $this->db->insert($table, $data);
  $var['CreatedID'] = $this->db->insert_id();
  $var['query'] = $query;
  return $var['CreatedID'];
}
public function update_query($table, $data, $key, $id) {
  $this->db->where($key, $id);
  $query = $this->db->update($table, $data);
  return $query;
}
public function update_ans_status($where_values) {
  $data = array('status' => 0);
  $this->db->update('answer', $data, $where_values);
    //$query = $this->db->update('answer', $data);
    //return $query;
}

public function get_sort_cat($asscat) {
  $sql="SELECT distinct(category.name),category.idcategory FROM `assessment_category` inner join category on assessment_category.category_id = category.idcategory where assessment_category.assessment_id = '$asscat' ";
  $query = $this->db->query($sql);
  return $query->result();
}

public function delete_submission($category_id,$user_id) {
  $sql="delete alpha FROM answer alpha join assessment_questions on assessment_questions.idassessment_questions=alpha.qid where assessment_questions.question_categoryid= '$category_id' and alpha.uid= '$user_id' ";
  $query = $this->db->query($sql);
            //return $query->result();
}

public function get_categories($id,$asscat) {
 $sql="SELECT * from assessment_questions where question_categoryid > $id and assessment_id= '$asscat' order by question_categoryid ";
 $query = $this->db->query($sql);
 $result = $query->result();
 if(count($result) != 0){
  return $result[0]->question_categoryid;
}
else{
  return "";
}

}

public function get_prev_category($id,$asscat) {
 $sql="SELECT * from assessment_questions where question_categoryid < $id and assessment_id= '$asscat' order by question_categoryid desc ";
 $query = $this->db->query($sql);
 $result = $query->result();
 if(count($result) != 0){
  return $result[0]->question_categoryid;
}
else{
  return "";
}

}
public function get_current_category($user_id) {
 $sql="SELECT current_category from user_session where user_id = $user_id ";
 $query = $this->db->query($sql);
 $result = $query->result();
 if(count($result) != 0){
  return $result[0]->current_category;
}
else{
  return "";
}

}
public function get_current_step($user_id) {
 $sql="SELECT current_step from user_session where user_id = $user_id ";
 $query = $this->db->query($sql);
 $result = $query->result();
 if(count($result) != 0){
  return $result[0]->current_step;
}
else{
  return "";
}

}

public function get_first_category($asscat,$current) {
  $sql="SELECT * from assessment_questions where assessment_id= '$asscat' and question_categoryid > $current ORDER BY question_categoryid  LIMIT 1 ";
  $query = $this->db->query($sql);
  $result = $query->result();
  if(count($result) != 0){
    return $result[0]->question_categoryid;
  }
  else{
    return "";
  }

}

public function check_data($user,$pass,$assessment_id)
{
  $sql="SELECT * from registration where username = '$user' and password = '$pass' and assessment_id = '$assessment_id' and status = 0 ";
  $query = $this->db->query($sql);
  return $query->result();
}
public function check_user($user)
{
  $sql="SELECT * from registration where username = '$user'";
  $query = $this->db->query($sql);
  return $query->result();
}

public function get_assessment_groups($id)
{
  $sql="SELECT assessment_group.*,`groups`.* FROM assessment_group JOIN `groups` ON assessment_group.group_id = `groups`.idgroup where assessment_id = '$id' GROUP BY assessment_group.group_id";
  $query = $this->db->query($sql);
  return $query->result();
}

public function get_general_questions($assesment_id)
{
  if($assesment_id <= 56)
  {
     $sql="SELECT * from general_comments WHERE idcomments < 6 ";
  }
  else
  {
     $sql="SELECT * from general_comments WHERE idcomments > 5";
  }
  $query = $this->db->query($sql);
  return $query->result();
}
public function get_client_name($assact){
  $sql = "SELECT clients.name,assessment.Title,clients.image FROM `assessment` join `clients` on assessment.Client_id = clients.idClients where assessment.idassessment = '$assact'";
  $query = $this->db->query($sql);
  $result =  $query->result();
  return $result;
}

/*public function total_step($assessment_id)
{
    $sql ="SELECT COUNT(DISTINCT category_id) as counter FROM `assessment_category` where `assessment_id`= '$assessment_id'";
    $query = $this->db->query($sql);
    $result =  $query->result();
    return $result[0]->counter;
  }*/

  public function total_step($assessment_id)
  {
    $sql ="SELECT COUNT(DISTINCT question_categoryid) as counter FROM `assessment_questions` where `assessment_id`= '$assessment_id'";
    $query = $this->db->query($sql);
    $result =  $query->result();
    return $result[0]->counter;
  }

  public function question_count($category_id,$assessment_id)
  {
    $sql ="select count(*) as counter from assessment_questions where question_categoryid= '$category_id' and assessment_id= '$assessment_id' ";
    $query = $this->db->query($sql);
    $result =  $query->result();
    return $result[0]->counter;
  }

  public function question_number($user_id)
  {
    $sql ="SELECT question_number as counter FROM `user_session` where user_id= '$user_id' ";
    $query = $this->db->query($sql);
    $result =  $query->result();
    return $result[0]->counter;
  }

  public function get_category_name($category_id)
  {
    if ($category_id == '') {
      $category_id = 12;
    }
    $sql="SELECT name as thename from category where idcategory= '$category_id' ";
    $query = $this->db->query($sql);
    /*$var=$query->result();
    return $var[0]->thename;*/
    $var=$query->row();
    return $var->thename;
  }

  public function get_current_question($user_id)
  {
    $sql ="SELECT question_number as question from user_session where user_id= '$user_id'";
    $query = $this->db->query($sql);
    $result =  $query->result();
    return $result[0]->question;
  }

  public function update_session($user_id) 
  {
    $sql1 ="select max_step,max_question,user_id from user_session where user_id= '$user_id'";
    $query1 = $this->db->query($sql1);
    $result1 =  $query1->result();
    $max_step =  $result1[0]->max_step;
    $max_question = $result1[0]->max_question;
    if ( $max_step == 0 && $max_question == 0) {

    } else {

      $sql="UPDATE user_session t, (select max_step,max_question,user_id from user_session where user_id= '$user_id' ) t1 SET t.current_step = t1.max_step,t.question_number = t1.max_question WHERE t.user_id = t1.user_id AND t.user_id = '$user_id' ";
      $query = $this->db->query($sql);
            //return $query->result();
    }
  }

  /* REPORT GENERATE*/
  public function get_report($id,$assessment_end_date) 
  {
    if($assessment_end_date > "2018-11-27")
      {
        $sql="SELECT ans.qid,r.assessment_id,r.id,r.username_show,r.username,r.password,cs.sort_sequence,g.name AS group_name,c.name AS category_name,a.Title AS assessment_title,aq.question_text,ans.answerid,aq.question_categoryid
        FROM registration r
        JOIN `groups` g ON g.idgroup = r.group
        JOIN assessment a ON r.assessment_id = a.idassessment
        JOIN assessment_questions aq ON aq.assessment_id = a.idassessment
        JOIN category c ON aq.question_categoryid = c.idcategory
        JOIN answer ans ON ans.qid = aq.idassessment_questions AND r.`id` = ans.`uid`
        JOIN category_sorting cs ON cs.client_id = r.id
        WHERE r.assessment_id = $id AND ans.status = 1 
        ORDER BY aq.question_categoryid,ans.qid ASC ";
      }
      else
      {
        $sql="SELECT ans.qid,r.assessment_id,r.id,r.username_show,r.username,r.password,cs.sort_sequence,g.name AS group_name,c.name AS category_name,a.Title AS assessment_title,aq.question_text,ans.answerid
        FROM registration r
        JOIN `groups` g ON g.idgroup = r.group
        JOIN assessment a ON r.assessment_id = a.idassessment
        JOIN assessment_questions aq ON aq.assessment_id = a.idassessment
        JOIN category c ON aq.question_categoryid = c.idcategory
        JOIN answer ans ON ans.qid = aq.idassessment_questions AND r.`id` = ans.`uid`
        JOIN category_sorting cs ON cs.client_id = r.id
        WHERE r.assessment_id = $id AND ans.status = 1
        ORDER BY ans.qid ASC ";
      }
    //echo $sql;die;
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function get_distinct_user_report($id) 
  {
    $sql="SELECT r.assessment_id,r.id,r.username,r.password,cs.sort_sequence,g.name AS group_name,c.name AS category_name,a.Title AS assessment_title,aq.question_text,ans.answerid
    FROM registration r
    JOIN `groups` g ON g.idgroup = r.group
    JOIN assessment a ON r.assessment_id = a.idassessment
    JOIN assessment_questions aq ON aq.assessment_id = a.idassessment
    JOIN category c ON aq.question_categoryid = c.idcategory
    JOIN answer ans ON ans.qid = aq.idassessment_questions AND r.`id` = ans.`uid`
    JOIN category_sorting cs ON cs.client_id = r.id
    WHERE r.assessment_id = $id AND ans.status = 1
    GROUP BY id
    ORDER BY username ";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function get_assessment_questions($id=''){
    $sql="SELECT COUNT(`idassessment_questions`) AS qcount FROM assessment_questions  WHERE `assessment_id` = $id";
    $query = $this->db->query($sql);
    $ret = $query->row();
    return $ret->qcount;
  }
  public function get_assessment_and_questions($id=''){
    $sql="SELECT c.name,COUNT(aq.`idassessment_questions`) AS qcount 
    FROM assessment_questions aq
    JOIN category c ON c.`idcategory` = aq.`question_categoryid`  
    WHERE aq.`assessment_id` = $id 
    GROUP BY aq.`question_categoryid`";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function delete_comment($where)
  {
    $this->db->where($where);
    $this->db->delete('user_comments');

  }


  /********************NEW EXPORT************************/
  public function get_top_header($assessment_id=''){
    $sql="SET SESSION group_concat_max_len = 1000000";
    $query = $this->db->query($sql);
    $sql="
    SELECT 
    GROUP_CONCAT(DISTINCT(nt.name),\"=>\",nt.total_questions SEPARATOR \",\") AS total_questions,
    GROUP_CONCAT(DISTINCT(nt.name) SEPARATOR \"=>\") AS comments_cat,COUNT(DISTINCT(nt.name)) AS total_ranking,
    (SELECT 
    GROUP_CONCAT(question SEPARATOR \"=>\") FROM general_comments
    ) AS general_comments
    FROM assessment_questions aq1
    JOIN (SELECT c.name,aq.question_categoryid,COUNT(aq.idassessment_questions) - 1 AS total_questions
    FROM assessment_questions aq 
    JOIN category c 
    ON aq.question_categoryid = c.idcategory WHERE aq.assessment_id = $assessment_id
    GROUP BY aq.question_categoryid
    ) nt";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public function get_details($assessment_id='') {
    $sql="SET SESSION group_concat_max_len = 1000000";
    $query = $this->db->query($sql);
    $sql="
    SELECT r.username,r.password,
    (SELECT COUNT(idcomments) FROM general_comments) AS total_general_comments,
    (SELECT COUNT(assessment_cat_id) FROM assessment_category WHERE assessment_id = $assessment_id) AS total_categories,
    (SELECT COUNT(idassessment_questions) FROM assessment_questions WHERE assessment_id = $assessment_id) AS total_questions,
    g.name AS group_name, 
    GROUP_CONCAT(
    CASE WHEN a.answerid = 1 THEN 'NA'
    WHEN a.answerid = 2 THEN 'DK'
    WHEN a.answerid = 3 THEN 'SD'
    WHEN a.answerid = 4 THEN 'D'
    WHEN a.answerid = 5 THEN 'SWD'
    WHEN a.answerid = 6 THEN 'SWA'
    WHEN a.answerid = 7 THEN 'A'
    WHEN a.answerid = 8 THEN 'SA'
    END SEPARATOR \"=>\") AS answers,cs.sort_sequence,
    GROUP_CONCAT(DISTINCT(uc.category_id),\"|\",uc.comment  SEPARATOR \"=>\") AS user_comments,
    GROUP_CONCAT(DISTINCT(gca.question_id),\"|\",gca.answer  SEPARATOR \"=>\") AS general_comments
    FROM answer a 
    LEFT JOIN registration r ON a.uid = r.id
    LEFT JOIN `groups` g ON r.group = g.idgroup
    LEFT JOIN assessment_questions aq ON a.qid = aq.idassessment_questions
    LEFT JOIN category c ON aq.question_categoryid = c.idcategory
    LEFT JOIN category_sorting cs ON r.id = cs.client_id
    LEFT JOIN user_comments uc ON r.id = uc.user_id
    LEFT JOIN category c1 ON uc.category_id = c1.idcategory
    LEFT JOIN general_comments_answers gca ON r.id = gca.client_id
    WHERE r.assessment_id = 11
    GROUP BY a.uid";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public function get_sasti_details($assessment_id='')
  {
    $sql="SELECT r.id,r.username,r.password,g.name
    FROM registration r
    JOIN `groups` g ON r.`group` = g.`idgroup`
    WHERE r.`assessment_id` = $assessment_id AND `registration_status` = 1";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public function check_if_sort_submitted($id='')
  {
    $sql="SELECT * from category_sorting where client_id = $id";
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0)
{
   return true;
} else {
  return false;
}
  }


  /*public function get_details($assessment_id='') {
    $sql="SET SESSION group_concat_max_len = 1000000";
    $query = $this->db->query($sql);
    $sql="
    SELECT r.username,r.password,
    (SELECT COUNT(idcomments) FROM general_comments) AS total_general_comments,
    (SELECT COUNT(assessment_cat_id) FROM assessment_category WHERE assessment_id = $assessment_id) AS total_categories,
    (SELECT COUNT(idassessment_questions) FROM assessment_questions WHERE assessment_id = $assessment_id) AS total_questions,
    g.name AS group_name, 
    GROUP_CONCAT(
    CASE WHEN a.answerid = 1 THEN 'NA'
    WHEN a.answerid = 2 THEN 'DK'
    WHEN a.answerid = 3 THEN 'SD'
    WHEN a.answerid = 4 THEN 'D'
    WHEN a.answerid = 5 THEN 'SWD'
    WHEN a.answerid = 6 THEN 'SWA'
    WHEN a.answerid = 7 THEN 'A'
    WHEN a.answerid = 8 THEN 'SA'
    END SEPARATOR \"=>\") AS answers,cs.sort_sequence,
    GROUP_CONCAT(DISTINCT(uc.category_id),\"|\",uc.comment  SEPARATOR \"=>\") AS user_comments,
    GROUP_CONCAT(DISTINCT(gca.question_id),\"|\",gca.answer  SEPARATOR \"=>\") AS general_comments
    FROM answer a 
    LEFT JOIN registration r ON a.uid = r.id
    LEFT JOIN groups g ON r.group = g.idgroup
    LEFT JOIN assessment_questions aq ON a.qid = aq.idassessment_questions
    LEFT JOIN category c ON aq.question_categoryid = c.idcategory
    LEFT JOIN category_sorting cs ON r.id = cs.client_id
    LEFT JOIN user_comments uc ON r.id = uc.user_id
    LEFT JOIN category c1 ON uc.category_id = c1.idcategory
    LEFT JOIN general_comments_answers gca ON r.id = gca.client_id
    WHERE r.assessment_id = 11
    GROUP BY a.uid";
    $query = $this->db->query($sql);
    return $query->result();
  }*/

public function get_user_comments($assessment_id='')
{
  $sql="SELECT c.name, GROUP_CONCAT(uc.comment SEPARATOR \"=>\") as the_comment
  FROM user_comments uc
JOIN category c ON c.idcategory = uc.category_id
 WHERE uc.assessment_id = $assessment_id
 GROUP BY c.name";
    $query = $this->db->query($sql);
    return $query->result();
}

public function gen_answers($assessment_id)
{

  $sql=" SELECT gc.idcomments,gc.question,GROUP_CONCAT(gca.answer SEPARATOR \"=>\") AS answers FROM general_comments_answers gca
JOIN registration r ON r.id = gca.client_id
JOIN general_comments gc ON gc.idcomments = gca.question_id 
WHERE r.assessment_id = $assessment_id AND registration_status = 1
GROUP BY gc.idcomments";
    $query = $this->db->query($sql);
    return $query->result();

}



}



/*
SET SESSION group_concat_max_len = 1000000;
SELECT r.id,r.username,r.password,g.name AS group_name, 
GROUP_CONCAT(c.name,',',aq.`idassessment_questions`,',',a.`answerid` SEPARATOR "=>"),cs.`sort_sequence`,
GROUP_CONCAT(c1.name,',',uc.comment  SEPARATOR "=>"),
GROUP_CONCAT(gca.`question_id`,',',gca.answer  SEPARATOR "=>")
FROM answer a 
LEFT JOIN registration r ON a.`uid` = r.`id`
LEFT JOIN groups g ON r.`group` = g.`idgroup`
LEFT JOIN assessment_questions aq ON a.`qid` = aq.`idassessment_questions`
LEFT JOIN category c ON aq.`question_categoryid` = c.`idcategory`
LEFT JOIN category_sorting cs ON r.`id` = cs.`client_id`
LEFT JOIN user_comments uc ON r.`id` = uc.`user_id`
LEFT JOIN category c1 ON uc.`category_id` = c1.`idcategory`
LEFT JOIN `general_comments_answers` gca ON r.`id` = gca.`client_id`
WHERE r.`assessment_id` = 11
GROUP BY a.`uid`



SET SESSION group_concat_max_len = 1000000;
SELECT r.id,r.username,r.password,g.name AS group_name, 
GROUP_CONCAT(c.name,',',aq.`idassessment_questions`,',',
CASE WHEN a.`answerid` = 1 THEN 'NA'
WHEN a.`answerid` = 2 THEN 'DK'
WHEN a.`answerid` = 3 THEN 'SD'
WHEN a.`answerid` = 4 THEN 'D'
WHEN a.`answerid` = 5 THEN 'SWD'
WHEN a.`answerid` = 6 THEN 'SWA'
WHEN a.`answerid` = 7 THEN 'A'
WHEN a.`answerid` = 8 THEN 'SA'
END SEPARATOR "=>"),cs.`sort_sequence`,
GROUP_CONCAT(c1.name,',',uc.comment  SEPARATOR "=>"),
GROUP_CONCAT(gca.`question_id`,',',gca.answer  SEPARATOR "=>")
FROM answer a 
LEFT JOIN registration r ON a.`uid` = r.`id`
LEFT JOIN groups g ON r.`group` = g.`idgroup`
LEFT JOIN assessment_questions aq ON a.`qid` = aq.`idassessment_questions`
LEFT JOIN category c ON aq.`question_categoryid` = c.`idcategory`
LEFT JOIN category_sorting cs ON r.`id` = cs.`client_id`
LEFT JOIN user_comments uc ON r.`id` = uc.`user_id`
LEFT JOIN category c1 ON uc.`category_id` = c1.`idcategory`
LEFT JOIN `general_comments_answers` gca ON r.`id` = gca.`client_id`
WHERE r.`assessment_id` = 11
GROUP BY a.`uid`
*/


/*SET SESSION group_concat_max_len = 1000000;
SELECT r.id,r.username,r.password,
(SELECT COUNT(`idcomments`) FROM `general_comments`) AS total_general_comments,
(SELECT COUNT(`assessment_cat_id`) FROM `assessment_category` WHERE `assessment_id` = 11) AS total_categories,g.name AS group_name, 
GROUP_CONCAT(
CASE WHEN a.`answerid` = 1 THEN 'NA'
WHEN a.`answerid` = 2 THEN 'DK'
WHEN a.`answerid` = 3 THEN 'SD'
WHEN a.`answerid` = 4 THEN 'D'
WHEN a.`answerid` = 5 THEN 'SWD'
WHEN a.`answerid` = 6 THEN 'SWA'
WHEN a.`answerid` = 7 THEN 'A'
WHEN a.`answerid` = 8 THEN 'SA'
END SEPARATOR "=>") AS answers,cs.`sort_sequence`,
GROUP_CONCAT(DISTINCT(c.name),',',uc.comment  SEPARATOR "=>") AS user_comments,
GROUP_CONCAT(DISTINCT(gca.`question_id`),',',gca.answer  SEPARATOR "=>") AS general_comments
FROM answer a 
LEFT JOIN registration r ON a.`uid` = r.`id`
LEFT JOIN groups g ON r.`group` = g.`idgroup`
LEFT JOIN assessment_questions aq ON a.`qid` = aq.`idassessment_questions`
LEFT JOIN category c ON aq.`question_categoryid` = c.`idcategory`
LEFT JOIN category_sorting cs ON r.`id` = cs.`client_id`
LEFT JOIN user_comments uc ON r.`id` = uc.`user_id`
LEFT JOIN category c1 ON uc.`category_id` = c1.`idcategory`
LEFT JOIN `general_comments_answers` gca ON r.`id` = gca.`client_id`
WHERE r.`assessment_id` = 11
GROUP BY a.`uid`*/



/*SET SESSION group_concat_max_len = 1000000;
SELECT r.id,r.username,r.password,
(SELECT COUNT(`idcomments`) FROM `general_comments`) AS total_general_comments,
(SELECT COUNT(`assessment_cat_id`) FROM `assessment_category` WHERE `assessment_id` = 11) AS total_categories,g.name AS group_name, 
GROUP_CONCAT(
CASE WHEN a.`answerid` = 1 THEN 'NA'
WHEN a.`answerid` = 2 THEN 'DK'
WHEN a.`answerid` = 3 THEN 'SD'
WHEN a.`answerid` = 4 THEN 'D'
WHEN a.`answerid` = 5 THEN 'SWD'
WHEN a.`answerid` = 6 THEN 'SWA'
WHEN a.`answerid` = 7 THEN 'A'
WHEN a.`answerid` = 8 THEN 'SA'
END SEPARATOR "=>") AS answers,cs.`sort_sequence`,
GROUP_CONCAT(DISTINCT(c.name),',',uc.comment  SEPARATOR "=>") AS user_comments,
GROUP_CONCAT(DISTINCT(gc.`idcomments`),',',
CASE WHEN gca.answer = '' THEN 'NA'
WHEN gca.answer != '' THEN gca.answer END
SEPARATOR "=>") AS general_comments
FROM answer a 
LEFT JOIN registration r ON a.`uid` = r.`id`
LEFT JOIN groups g ON r.`group` = g.`idgroup`
LEFT JOIN assessment_questions aq ON a.`qid` = aq.`idassessment_questions`
LEFT JOIN category c ON aq.`question_categoryid` = c.`idcategory`
LEFT JOIN category_sorting cs ON r.`id` = cs.`client_id`
LEFT JOIN user_comments uc ON r.`id` = uc.`user_id`
LEFT JOIN category c1 ON uc.`category_id` = c1.`idcategory`
LEFT JOIN `general_comments_answers` gca ON r.`id` = gca.`client_id`
CROSS JOIN `general_comments` gc  
WHERE r.`assessment_id` = 11
GROUP BY a.`uid`*/



