<?php 
class Query extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    public function query($query) {
        $query = $this->db->query($query);
        return $query->result();
    }

    public function login($Email, $PasswordEncrypted) {

        $sql = "SELECT * FROM user WHERE email = '$Email' AND password = '$PasswordEncrypted' AND status = 1 LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insert_query($table, $data) {
        $query = $this->db->insert($table, $data);
        $var['CreatedID'] = $this->db->insert_id();
        $var['query'] = $query;
        return $var;
    }

    public function insert_batch_query($table, $data) {

        $query = $this->db->insert_batch($table, $data);
        $var['CreatedID'] = $this->db->insert_id();
        $var['query'] = $query;
        return $var;
    }

    public function get_industries() {

        $sql = "select * from industries where status = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function update_query($table, $data, $key, $id) {
        $this->db->where($key, $id);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function get_all_clients() {

        $sql = "select clients.*,COUNT(idassessment) as counting from clients left JOIN assessment ON clients.idClients = assessment.client_id where status = 1 GROUP BY clients.idClients order by created_date desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_all_categories() {

        $sql = "SELECT * FROM category where status = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_all_questions() {

        $sql = "SELECT * FROM questions,category where questions.category_id=category.idcategory and questions.status= '1' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_current_questions($assess_id) {

        $sql = "SELECT question_text,name,idassessment_questions,assessment_questions.assessment_id from assessment_questions join category on assessment_questions.question_categoryid = category.idcategory where assessment_questions.assessment_id= '$assess_id' order by category.idcategory ASC";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_all_groups() {

        $sql = "SELECT * FROM `groups`";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_client_by_id($client_id) {

        $sql = "SELECT * FROM clients where idClients=$client_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_client_image_by_id($client_id) {

        $sql = "SELECT image FROM clients where idClients=$client_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function client_assessment_count($client_id) {

        $sql = "SELECT count(*) as countvalue FROM assessment where Client_id=$client_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_category_by_id($category_id) {

        $sql = "SELECT * FROM category where idcategory=$category_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_question_by_id($question_id) {

        $sql = "SELECT * FROM questions where status='1' and idquestions=$question_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_question_by_id_assessment($question_id) {

        $sql = "SELECT * FROM questions where status='1' and category_id=$question_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_group_by_id($group_id) {

        $sql = "SELECT * FROM `groups` where idgroup=$group_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function delete_question($Question_id) {

        $sql = "DELETE FROM assessment_questions where idassessment_questions= '$Question_id' ";
        $query = $this->db->query($sql);
        return true;
    }

    public function deactivate_assessment($assessment_id) {
        //2 for deactivated
        $sql = "UPDATE assessment SET is_generic= '2' WHERE idassessment= $assessment_id ";
        $query = $this->db->query($sql);
        return true;
    }
    
    public function disable_assessment($assessment_id) {
        //2 for deactivated
        $sql = "UPDATE assessment SET status_check= '1' WHERE idassessment= $assessment_id ";
        $query = $this->db->query($sql);
        return true;
    }

    public function activate_assessment($assessment_id) {
        //2 for deactivated
        $sql = "UPDATE assessment SET is_generic= '0' WHERE idassessment= $assessment_id ";
        $query = $this->db->query($sql);
        return true;
    }

    public function delete_group($group_id) {

        $sql = "DELETE FROM `groups` where idgroup= $group_id ";
        $query = $this->db->query($sql);
        return true;
    }

    public function delete_assessment_questions($assessment_id) {

        $sql = "DELETE FROM assessment_questions where assessment_id= $assessment_id ";
        $query = $this->db->query($sql);
        return true;
    }

    public function get_edit_image($Assessment_id) {

        $sql = "SELECT * FROM clients join assessment on assessment.Client_id=clients.idClients where assessment.idassessment= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_assesment_details($Assessment_id) {

        $sql = "SELECT Title FROM assessment where idassessment= $Assessment_id ";
        $query = $this->db->query($sql);
        $ret = $query->row();
        return $ret->Title;
    }

    public function get_assesment($Assessment_id) {

        $sql = "SELECT * FROM assessment where idassessment= $Assessment_id ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_assesment_end_date($Assessment_id) {

        $sql = "SELECT enddate FROM assessment where idassessment= $Assessment_id ";
        $query = $this->db->query($sql);
        $ret = $query->row();
        return $ret->enddate;
    }

    public function get_assesment_label_id($Assessment_id) {

        $sql = "SELECT label_id FROM assessment where idassessment= $Assessment_id ";
        $query = $this->db->query($sql);
        $ret = $query->row();
        return $ret->label_id;
    }
    
    public function get_assesment_label() {

        $sql = "SELECT * FROM assessment_label where status = 1 ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function update_assessment_label($assessment_id, $label) {

        $sql = "UPDATE assessment SET label_id= '$label' WHERE idassessment= $assessment_id ";
        $query = $this->db->query($sql);
    }

    public function update_assessment_enddate($assessment_id, $date) {

        $sql = "UPDATE assessment SET status_check=0,enddate= '$date' WHERE idassessment= $assessment_id ";
        $query = $this->db->query($sql);
    }

    public function get_edit_questions($Assessment_id) {

        $sql = "SELECT * FROM assessment_questions where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_copy_edit_questions($str) {

        $sql = "SELECT * FROM assessment_questions where $str ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_edit_category($Assessment_id) {

        $sql = "SELECT distinct(category.name),category.idcategory FROM assessment_category  join category on assessment_category.category_id=category.idcategory where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_edit_groups($Assessment_id) {

        $sql = "SELECT * from assessment_group where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_all_assessments() {

        //$sql = "SELECT * FROM assessment inner join clients on assessment.Client_id=clients.idClients where assessment.enddate>=curdate() order by assessment.startdate desc";
        $sql = "SELECT assessment.*,clients.*,assessment.status_check as status_check FROM assessment inner join clients on assessment.Client_id=clients.idClients order by assessment.enddate desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_completed_assessments() {

        $sql = "SELECT * FROM assessment inner join clients on assessment.Client_id=clients.idClients where assessment.status_check = 1 order by assessment.enddate desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_editing_category($Assessment_id) {

        $sql = "SELECT * from assessment_category where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function change_status($user_id) {

        $sql = "UPDATE registration SET status= 1 WHERE id= '$user_id' ";
        $query = $this->db->query($sql);
    }

    public function delete_client_by_id($idClients) {

        $sql = "UPDATE clients SET status= 3 WHERE idClients= '$idClients' ";
        $this->db->query($sql);
    }
    
    public function delete_category_by_id($idCategory) {

        $sql = "UPDATE category SET status= 3 WHERE idcategory= '$idCategory' ";
        $query = $this->db->query($sql);
    }

    public function delete_user($user_id) {

        $sql = "UPDATE registration SET assessment_id = CONCAT('delete_', assessment_id) WHERE `id`=$user_id;";
        //$sql = "DELETE FROM `registration` WHERE `id`=$user_id;";
        $query = $this->db->query($sql);
    }

    public function assessment_status($Assessment_id) {

        $sql = "SELECT * FROM registration join `groups` on `groups`.idgroup=registration.group where registration.assessment_id= '$Assessment_id' and registration.registration_status = 1 ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function assessment_export_question($Assessment_id) {

        $sql = "SELECT category.name,assessment_questions.question_text FROM assessment_questions
JOIN category ON assessment_questions.question_categoryid = category.idcategory
WHERE assessment_questions.assessment_id = $Assessment_id ORDER BY assessment_questions.question_categoryid";
        $query = $this->db->query($sql);
        return $query->result();
    }

}

?>