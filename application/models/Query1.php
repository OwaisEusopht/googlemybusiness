<?php 

class Query extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }
    
    public function query($query) {
        $query = $this->db->query($query);
        return $query->result();
    }

    public function login($Email, $PasswordEncrypted) {

        $sql = "SELECT * FROM user WHERE email = '$Email' AND password = '$PasswordEncrypted' AND status = 1 LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function insert_query($table, $data) {
        $query = $this->db->insert($table, $data);
        $var['CreatedID'] = $this->db->insert_id();
        $var['query'] = $query;
        return $var;
    }
	public function insert_batch_query($table, $data) {

        $query = $this->db->insert_batch($table, $data);
        $var['CreatedID'] = $this->db->insert_id();
        $var['query'] = $query;
        return $var;
    }
	

    public function update_query($table, $data, $key, $id) {
        $this->db->where($key, $id);
        $query = $this->db->update($table, $data);
        return $query;
    }
    
    public function get_all_clients() {
        
        $sql = "select clients.*,COUNT(idassessment) as counting from clients left JOIN assessment ON clients.idClients = assessment.client_id GROUP BY clients.idClients order by created_date desc";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	public function get_all_categories() {
        
        $sql = "SELECT * FROM category";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    public function get_all_questions() {
        
        $sql = "SELECT * FROM questions,category where questions.category_id=category.idcategory";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	public function get_current_questions($assess_id) {
        
        $sql = "SELECT question_text,name,idassessment_questions,assessment_questions.assessment_id from assessment_questions join assessment_category ON assessment_questions.question_categoryid = assessment_category.assessment_cat_id join category on assessment_category.category_id = category.idcategory where assessment_questions.assessment_id='$assess_id' order by category.name";
        
		$query = $this->db->query($sql);
        return $query->result();
        
    }

	public function get_all_groups() {
        
        $sql = "SELECT * FROM groups";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function get_client_by_id($client_id) {
        
        $sql = "SELECT * FROM clients where idClients=$client_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function get_client_image_by_id($client_id) {
        
        $sql = "SELECT image FROM clients where idClients=$client_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
	public function client_assessment_count($client_id) {
        
        $sql = "SELECT count(*) as countvalue FROM assessment where Client_id=$client_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function get_category_by_id($category_id) {
        
        $sql = "SELECT * FROM category where idcategory=$category_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function get_question_by_id($question_id) {
        
        $sql = "SELECT * FROM questions where idquestions=$question_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function get_group_by_id($group_id) {
        
        $sql = "SELECT * FROM groups where idgroup=$group_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function delete_question($Question_id) {
        
        $sql = "DELETE FROM assessment_questions where idassessment_questions= '$Question_id' ";
        $query = $this->db->query($sql);
        return true;
        
    }
	
    public function get_edit_image($Assessment_id) {
        
        $sql = "SELECT * FROM clients join assessment on assessment.Client_id=clients.idClients where assessment.idassessment= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
    public function get_edit_questions($Assessment_id) {
        
        $sql = "SELECT * FROM assessment_questions where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
	public function get_edit_category($Assessment_id) {
        
        $sql = "SELECT distinct(category.name),category.idcategory FROM assessment_category  join category on assessment_category.category_id=category.idcategory where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
	public function get_edit_groups($Assessment_id) {
        
        $sql = "SELECT * from assessment_group where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
		
       
        
    }
	public function get_all_assessments() {
        
        $sql = "SELECT * FROM assessment inner join clients on assessment.Client_id=clients.idClients order by assessment.startdate desc";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
	public function get_editing_category($Assessment_id) {
        
        $sql = "SELECT * from assessment_category where assessment_id= '$Assessment_id' ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
	public function change_status($user_id) {
        
        $sql = "UPDATE registration SET status= 1 WHERE id= '$user_id' ";
        $query = $this->db->query($sql);
        
        
    }
    

}

?>