<?php 
class asses_model extends CI_Model {

        public function __construct()
        {
            $this->load->database();
        }
        public function insert($table,$data)
		{
	       $this->db->insert($table, $data);
		}

        public function get_data($id,$aid) {
			$sql="SELECT * from assessment_questions where question_categoryid= '$id' and assessment_id= '$aid' ";
	     	$query = $this->db->query($sql);
        	return $query->result();
		}
		public function insert_query($table, $data) {
        $query = $this->db->insert($table, $data);
        $var['CreatedID'] = $this->db->insert_id();
        $var['query'] = $query;
        return $var['CreatedID'];
		}
		public function update_query($table, $data, $key, $id) {
        $this->db->where($key, $id);
        $query = $this->db->update($table, $data);
        return $query;
		}
		
		public function get_sort_cat($asscat) {
            $sql="SELECT distinct(category.name),category.idcategory FROM `assessment_category` inner join category on assessment_category.category_id = category.idcategory where assessment_category.assessment_id = '$asscat' ";
            $query = $this->db->query($sql);
            return $query->result();
        }

		public function get_categories($id,$asscat) {
			$sql="SELECT * from assessment_questions where question_categoryid > $id and assessment_id= '$asscat' order by question_categoryid ";
	     	$query = $this->db->query($sql);
        	$result = $query->result();
        	if(count($result) != 0){
        		return $result[0]->question_categoryid;
        	}
        	else{
        		return "";
        	}
        	
		}
		public function get_current_category($user_id) {
			$sql="SELECT current_category from user_session where user_id = $user_id ";
	     	$query = $this->db->query($sql);
        	$result = $query->result();
        	if(count($result) != 0){
        		return $result[0]->current_category;
        	}
        	else{
        		return "";
        	}
        	
		}
		public function get_current_step($user_id) {
			$sql="SELECT current_step from user_session where user_id = $user_id ";
	     	$query = $this->db->query($sql);
        	$result = $query->result();
        	if(count($result) != 0){
        		return $result[0]->current_step;
        	}
        	else{
        		return "";
        	}
        	
		}

        public function get_first_category($asscat,$current) {
            $sql="SELECT * from assessment_questions where assessment_id= '$asscat' and question_categoryid > $current  LIMIT 1 ";
            $query = $this->db->query($sql);
            $result = $query->result();
            if(count($result) != 0){
                return $result[0]->question_categoryid;
            }
            else{
                return "";
            }
            
        }

        public function check_data($user,$pass,$assessment_id)
        {
            $sql="SELECT * from registration where username = '$user' and password = '$pass' and assessment_id = '$assessment_id' and status = 0 ";
            $query = $this->db->query($sql);
            return $query->result();
    }
    public function check_user($user)
    {
        $sql="SELECT * from registration where username = '$user'";
            $query = $this->db->query($sql);
            return $query->result();
    }
	
	public function get_assessment_groups($id)
    {
        $sql="SELECT assessment_group.*,groups.* FROM assessment_group JOIN groups ON assessment_group.group_id = groups.idgroup where assessment_id = '$id' GROUP BY assessment_group.group_id";
            $query = $this->db->query($sql);
            return $query->result();
    }
	
	public function get_general_questions()
    {
        $sql="SELECT * from general_comments";
            $query = $this->db->query($sql);
            return $query->result();
    }
	public function get_client_name($assact){
        $sql = "SELECT clients.name,assessment.Title,clients.image FROM `assessment` join `clients` on assessment.Client_id = clients.idClients where assessment.idassessment = '$assact'";
        $query = $this->db->query($sql);
        $result =  $query->result();
        return $result;
	}

	public function total_step($assessment_id)
    {
        $sql ="SELECT COUNT(DISTINCT category_id) as counter FROM `assessment_category` where `assessment_id`= '$assessment_id'";
        $query = $this->db->query($sql);
        $result =  $query->result();
        return $result[0]->counter;
    }
    	public function get_category_name($category_id)
    {
        $sql="SELECT name as thename from category where idcategory= '$category_id' ";
            $query = $this->db->query($sql);
            $var=$query->result();
			return $var[0]->thename;
    }
    
    	public function get_current_question($user_id)
    {
        $sql ="SELECT question_number as question from user_session where user_id= '$user_id'";
        $query = $this->db->query($sql);
        $result =  $query->result();
        return $result[0]->question;
    }

}