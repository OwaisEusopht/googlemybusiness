<br/>
<br/>
<table class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <th><div><?php echo get_phrase('location_name'); ?></div></th>
            <th><div><?php echo get_phrase('store_code'); ?></div></th>
            <th><div><?php echo get_phrase('name'); ?></div></th>
            <th><div><?php echo get_phrase('comment'); ?></div></th>
            <th><div><?php echo get_phrase('rating'); ?></div></th>
            <th><div><?php echo get_phrase('created_time'); ?></div></th>
            <th><div><?php echo get_phrase('reply'); ?></div></th>
            <th><div><?php echo get_phrase('reply_time'); ?></div></th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        <?php
        for ($i=0; $i< count($allReviews); $i++){
            for($j=0;$j<count($allReviews[$i]);$j++){
            ?>

            <tr>
                <td><?php echo $locationName[$i]['locationName'] ?></td>
                <td><?php echo $locationName[$i]['storeCode'] ?></td>
                <td><?php echo $allReviews[$i][$j]['reviewer']['displayName']; ?></td>
                <td><?php echo $allReviews[$i][$j]['comment'] ?></td>
                <td><?php echo $allReviews[$i][$j]['starRating'] ?></td>
                <td><?php echo $allReviews[$i][$j]['createTime'] ?></td>
                <td><?php echo $allReviews[$i][$j]['reviewReply']['comment']; ?></td>
                <td><?php echo $allReviews[$i][$j]['reviewReply']['updateTime']; ?></td>
                <td>
                    <a class="btn btn-primary" href="<?= base_url().'index.php?admin/review_reply/?review='.$allReviews[$i][$j]['name'] ?>" >Reply</a>
                </td>
            </tr>
        <?php 
            }
        }
        ?>
    </tbody>
</table>

<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function ($) {

        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(3, false);
                            this.fnPrint(true, oConfig);
                            window.print();
                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>



