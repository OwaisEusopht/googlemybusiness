<div class="sidebar-menu">
    <header class="logo-env" >
        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>index.php?admin/dashboard"">
            GMB
            </a>
        </div>
        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>
    <div style=""></div>
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                <i class="entypo-gauge"></i>
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>
        <?php $user_data = $this->session->userdata('user_data');
            if($user_data->access_token != ""){
            ?>
        <li class="<?php if ($page_name == 'account_locations') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/get_business">
                    <i class="entypo-list"></i>
                    <span><?php echo get_phrase('my_business'); ?></span>
                </a>
            </li>
            <li class="<?php if ($page_name == 'account_reviews') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/get_all_reviews">
                    <i class="entypo-list"></i>
                    <span><?php echo get_phrase('all_reviews'); ?></span>
                </a>
            </li>
            <?php
            }
        ?>
        <?php
        if ($this->session->userdata('level') == "100") {
            ?>
            <li class="<?php if ($page_name == 'users') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/users">
                    <i class="entypo-list"></i>
                    <span><?php echo get_phrase('user'); ?></span>
                </a>
            </li>
            <?php
        }
        ?>
        

        <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin/manage_profile">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>
    </ul>
</div>